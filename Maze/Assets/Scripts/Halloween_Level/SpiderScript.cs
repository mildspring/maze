﻿using UnityEngine;
using System.Collections;

public class SpiderScript : MonoBehaviour {
	private Vector3 startingPosition;

	public float allowedXRadius = 0.1f;
	public float allowedYRadius = 0.0f;
	public float allowedZRadius = 0.1f;

	public float restMinimumSeconds = 1;
	public float restMaximumSeconds = 1;

	private float destinationX;
	private float destinationY;
	private float destinationZ;

	private MoveLerpObject moveLerpObject;

	public float offsetYRotation = 0.0f;

	public string walkingAnimation = null;
	public string idleAnimation = null;

	// Use this for initialization
	void Start () {
		startingPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
		moveLerpObject = GetComponent<MoveLerpObject> ();
		SetDestination ();
	}

	private void SetDestination() {
		if (!string.IsNullOrEmpty(walkingAnimation)) {
			GetComponent<Animation>().Play (walkingAnimation);
		}

		destinationX = Random.Range (startingPosition.x - allowedXRadius, startingPosition.x + allowedXRadius);
		destinationY = Random.Range (startingPosition.y - allowedYRadius, startingPosition.y + allowedYRadius);
		destinationZ = Random.Range (startingPosition.z - allowedZRadius, startingPosition.z + allowedZRadius);

		if (moveLerpObject.enabled) {
			moveLerpObject.enabled = false;
		}

		moveLerpObject.finalPosition = new Vector3 (destinationX, destinationY, destinationZ);

		// rotate
		{
			// found here how to rotate:
			// http://gamedev.stackexchange.com/questions/32651/angle-between-two-points

			//find the vector pointing from our position to the target
			Vector3 Dir = (moveLerpObject.finalPosition - transform.position).normalized;
			
			//create the rotation to look at the target
			Quaternion Rotation = Quaternion.LookRotation(Dir);

			Rotation *= Quaternion.Euler(Vector3.up * offsetYRotation);

			transform.rotation = Rotation;
		}

		moveLerpObject.moveLerpObjectDone = MoveLerpObjectDone;
		//Debug.Log ("enabling moveLerpObject with final position: " + moveLerpObject.finalPosition + " starting position " + startingPosition);
		moveLerpObject.enabled = true;
	}

	private void MoveLerpObjectDone() {
		if (!string.IsNullOrEmpty(idleAnimation)) {
			GetComponent<Animation>().Play (idleAnimation);
		}
		//Debug.Log ("MoveLerpObjectDone got called");
		Invoke ("SetDestination", Random.Range(restMinimumSeconds, restMaximumSeconds));
		//SetDestination ();
	}
}

﻿using UnityEngine;

public class JackoLanternScript : MonoBehaviour {
	public float targetPosition = -0.12f;

	void OnTriggerEnter(Collider other) {
		Debug.Log ("lantern touched " + other.name);
		MoveLerpObject MoveLerpObject = GetComponent<MoveLerpObject>();
		MoveLerpObject.finalPosition = new Vector3 (transform.position.x, targetPosition, transform.position.z);
		MoveLerpObject.enabled = true;
	}
}

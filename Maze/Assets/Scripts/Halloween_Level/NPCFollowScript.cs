﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCFollowScript : MonoBehaviour {
	public Themes themeSpecific = Themes.None;
	public GameObject rootObject;
	public float restMinimumSeconds;
	public float restMaximumSeconds;

	public string idleAnimation;
	public string walkingAnimation;

	public float offsetYRotation = 0.0f;

	private LevelInfo levelInfo;
	private MoveLerpObject moveLerpObject;

	private List<Position> thePath;
	private int targetPathIdx;

	Position currentPosition;
	private Position goToPosition = new Position();

	// used for ghost to bypass walking in straight line
	public bool cutThroughWalls = false;
	// set to true if we do not want to follow player
	public bool randomPositionAlways = false;

	// Use this for initialization
	void Start () {
		if (themeSpecific != Themes.None && themeSpecific != Utility_CurrentTheme.CurrentTheme) {
			gameObject.SetActive(false);
			return;
		}

		levelInfo = rootObject.GetComponent<LevelInfo> ();
		moveLerpObject = GetComponent<MoveLerpObject> ();

//		Debug.Log ("NPCFollowScript: levelInfo " + ((levelInfo == null) ? "**NULL**" : "something"));
//		Debug.Log ("X: " + levelInfo.GridX);
//		Debug.Log ("Y: " + levelInfo.GridY);
//
		currentPosition = new Position (Mathf.RoundToInt (Random.Range (1, levelInfo.GridX)), Mathf.RoundToInt (Random.Range (1, levelInfo.GridY)));
		transform.position = new Vector3 (currentPosition.x, 0.0f, currentPosition.y + 0.5f);
		FindNewPath ();
	}

	private void FindNewPath() {
		if (randomPositionAlways) {
			goToPosition.x = Mathf.RoundToInt (Random.Range (1, levelInfo.GridX));
			goToPosition.y = Mathf.RoundToInt (Random.Range (1, levelInfo.GridY));
		}
		else {
			goToPosition.x = Mathf.RoundToInt (levelInfo.PlayerObject.transform.position.x);
			goToPosition.y = Mathf.RoundToInt (levelInfo.PlayerObject.transform.position.z - 0.5f);
		}

		while (goToPosition.Equals(currentPosition)) {
			goToPosition.x = Mathf.RoundToInt (Random.Range (1, levelInfo.GridX));
			goToPosition.y = Mathf.RoundToInt (Random.Range (1, levelInfo.GridY));
		}
		PathFinder PathFinder = new PathFinder (levelInfo.GridX, levelInfo.GridY, levelInfo.WallsLeft);

		thePath = PathFinder.findPath (currentPosition, goToPosition, false);
		targetPathIdx = thePath.Count - 1;
		SetDestination ();
	}

	private void SetDestination() {
		if (!string.IsNullOrEmpty(walkingAnimation) && !GetComponent<Animation>().IsPlaying(walkingAnimation)) {
			GetComponent<Animation>().Play (walkingAnimation);
		}

		int UsingIndex = this.targetPathIdx;
		if (cutThroughWalls) {
			UsingIndex = 0;
		}
		float DestinationX = thePath[UsingIndex].x;
		float DestinationZ = thePath[UsingIndex].y + 0.5f;
		
		if (moveLerpObject.enabled) {
			moveLerpObject.enabled = false;
		}
		
		moveLerpObject.finalPosition = new Vector3 (DestinationX, transform.position.y, DestinationZ);
		
		// rotate
		{
			// found here how to rotate:
			// http://gamedev.stackexchange.com/questions/32651/angle-between-two-points
			
			//find the vector pointing from our position to the target
			Vector3 Dir = (moveLerpObject.finalPosition - transform.position).normalized;
			
			//create the rotation to look at the target
			Quaternion Rotation = Quaternion.LookRotation(Dir);
			
			Rotation *= Quaternion.Euler(Vector3.up * offsetYRotation);

			transform.rotation = Rotation;
		}
		
		moveLerpObject.moveLerpObjectDone = MoveLerpObjectDone;
		//Debug.Log ("enabling moveLerpObject with final position: " + moveLerpObject.finalPosition + " starting position " + startingPosition);
		moveLerpObject.enabled = true;
	}
	
	private void MoveLerpObjectDone() {
		//currentPosition = new Position (Mathf.RoundToInt (Random.Range (1, levelInfo.GridX)), Mathf.RoundToInt (Random.Range (1, levelInfo.GridY)));
		//currentPosition = new Position (Mathf.RoundToInt (Random.Range (1, levelInfo.GridX)), Mathf.RoundToInt (Random.Range (1, levelInfo.GridY)));

		if (targetPathIdx == 0 || cutThroughWalls) {
			currentPosition = thePath[0];

			if (!string.IsNullOrEmpty(idleAnimation)) {
				GetComponent<Animation>().Play (idleAnimation);
			}

			//Debug.Log ("MoveLerpObjectDone got called");
			Invoke ("FindNewPath", Random.Range(restMinimumSeconds, restMaximumSeconds));
			//SetDestination ();
		}
		else {
			--targetPathIdx;
			SetDestination ();
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HalloweenLevelFinished : MonoBehaviour {
	public GameObject player;
	public List <GameObject> randomGameObjects;

	// Use this for initialization
	void Start () {
		if (Utility_CurrentTheme.CurrentTheme == Themes.Halloween) {
			int RandomGhost = Random.Range (0, randomGameObjects.Count);
			Debug.Log ("RandomGhost is: " + RandomGhost);
			GameObject GameObject = randomGameObjects [RandomGhost];

			GameObject.transform.position = new Vector3 (player.transform.position.x, GameObject.transform.position.y, player.transform.position.z);
			//GameObject.transform.rotation = new Quaternion (this.player.transform.position.x, this.player.transform.position.y, this.player.transform.position.z, 0);
			GameObject.transform.rotation = player.transform.rotation;
			GameObject.transform.position += transform.right * 1 * .15f;
			GameObject.transform.position += transform.forward * 1 * -.2f;
			GameObject.SetActive (true);
		}
	}

//	void OnEnable() {
//		Start ();
//	}
}

﻿using UnityEngine;

public class KeyScript : MonoBehaviour
{
	private bool keyEnabled = false;
	private PlayerScript playerScript;	

	private PlayerScript PlayerScript
	{
		get
		{
			if (playerScript == null)
			{
				playerScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScript> ();
			}
			return playerScript;
		}
	}

	public bool KeyEnabled
	{ 
		get
		{ 
			return keyEnabled; 
		} 
		set
		{ 
			keyEnabled = value;
			if (keyEnabled)
			{
				PlayerScript.PickUpKey ();
				if (Utility_Music.SoundOn)
				{
					GetComponent<AudioSource> ().Play ();
				}
			}
		} 
	}
}

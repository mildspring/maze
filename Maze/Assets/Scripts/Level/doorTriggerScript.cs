﻿using UnityEngine;
using System.Collections;

public class doorTriggerScript : MonoBehaviour {
	private GameObject door;
	public GameObject Door { get { return door; } set { door = value; } }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public bool DoorTriggered {
		get { return door.GetComponent<doorScript> ().DoorTriggered; }
		set { door.GetComponent<doorScript> ().DoorTriggered = value; }
	}
	public bool NeedsKey { get { return door.GetComponent<doorScript> ().NeedsKey; } }
}

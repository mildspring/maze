﻿using System.Collections.Generic;
using UnityEngine;

public delegate void CelebrationDone ();

public class PlayerScript : MonoBehaviour
{
	public int rotationDegreesPerSecond;
	public float rotationSpeed;
	public GameObject levelFinished;
	public GameObject key1;
	private bool leftButtonHeld = false;

	public bool LeftButtonHeld { get { return leftButtonHeld; } set { leftButtonHeld = value; } }

	private bool rightButtonHeld = false;

	public bool RightButtonHeld { get { return rightButtonHeld; } set { rightButtonHeld = value; } }

	private bool walkButtonToggled = false;

	public bool WalkButtonToggled { get { return walkButtonToggled; } set { walkButtonToggled = value; } }

	private bool celebrate = false;
	private bool celebrateOnce = false;
	private bool hitOnce = false;
	private CelebrationDone celebrationDone;
	private AudioSource walkingSound;
	public bool walkingSoundEnabled = false;
	public List<GameObject> keysInHand;

	// Use this for initialization
	void Start ()
	{
		walkingSound = GetComponent<AudioSource> ();
		LetGoKey();
	}
	
	// Update is called once per frame
	void Update ()
	{
		CharacterController controller = GetComponent<CharacterController> ();
		Vector3 moveDirection = transform.forward;
		
		bool Right = Input.GetKey (KeyCode.RightArrow) || rightButtonHeld;
		rightButtonHeld = false;
		bool Left = Input.GetKey (KeyCode.LeftArrow) || leftButtonHeld;
		leftButtonHeld = false;
		bool Up = Input.GetKey (KeyCode.UpArrow) || walkButtonToggled;

		if (celebrate)
		{
			if (celebrateOnce)
			{
				if (!GetComponent<Animation>().IsPlaying ("Jump"))
				{
					GetComponent<Animation>().Play ("Jump");
					celebrateOnce = false;
					hitOnce = true;
				}
			}
			else if (hitOnce)
			{
				if (!GetComponent<Animation>().IsPlaying ("Jump") && !GetComponent<Animation>().IsPlaying ("Attack_01"))
				{
					GetComponent<Animation>().Play ("Attack_01");
					hitOnce = false;
				}
			}
			else
			{
				if (!GetComponent<Animation>().IsPlaying ("Attack_01"))
				{
					celebrationDone ();
				}
			}
		}
		else
		{

			//KeyCode.RightArrow
			if (Right)
			{
				//controller.
				transform.Rotate (-1 * rotationDegreesPerSecond * Vector3.down * Time.deltaTime);
			}
			else if (Left)
			{
				transform.Rotate (rotationDegreesPerSecond * Vector3.down * Time.deltaTime);
			}


			if (Up)
			{
				if (Left || Right)
				{
					controller.Move (moveDirection * Time.deltaTime * rotationSpeed);
				}
				else
				{
					controller.Move (moveDirection * Time.deltaTime);
				}
				if (!GetComponent<Animation>().IsPlaying ("Run"))
				{
					GetComponent<Animation>().Play ("Run");
				}
				if (Utility_Music.SoundOn && walkingSoundEnabled && !walkingSound.isPlaying)
				{
					walkingSound.Play ();
				}
			}
			else
			{
				if (!GetComponent<Animation>().IsPlaying ("Idle_01"))
				{
					GetComponent<Animation>().Play ("Idle_01");
				}
			}
		}

		Camera c = transform.Find ("Camera").gameObject.GetComponent<Camera>();
		Vector3 target = new Vector3 (transform.position.x + 0.5f, transform.position.y, transform.position.z + 0.5f);
		Ray ray = c.ScreenPointToRay (target);
		Debug.DrawRay (ray.origin, /*ray.forward,*/ray.direction, Color.yellow);

		Vector3 gravityForce = new Vector3 (0, -20, 0);
		controller.Move (gravityForce * Time.deltaTime);

	}

	public void Celebrate (CelebrationDone CelebrationDone)
	{
		Debug.Log ("Playing Celebrate animation");
		celebrate = true;
		celebrateOnce = true;
		celebrationDone = CelebrationDone;
	}

	private void OnTriggerEnter (Collider other)
	{
		//Debug.Log("Character hit: " + hit.collider.name);
		DetectedCollisions(other);
	}

	private void OnControllerColliderHit(ControllerColliderHit hit)
	{
		//Debug.Log("Character hit: " + hit.collider.name);
		DetectedCollisions(hit.collider);
	}

	private void DetectedCollisions(Collider other)
    {
		if (other.gameObject.tag == "LastPos")
		{
			gameObject.transform.position = new Vector3(other.transform.position.x, gameObject.transform.position.y, other.transform.position.z);
			other.enabled = false;
			Debug.Log("Touching LastPos");
			System.TimeSpan TotalRun = Utility_LevelTimer.FinishLevel();
			levelFinished.SetActive(true);
		}
		else if (other.gameObject.name == "doorTrigger(Clone)")
		{
			Debug.Log("Touching doorTrigger");
			if (other.gameObject.GetComponent<doorTriggerScript>().NeedsKey && !key1.GetComponent<KeyScript>().KeyEnabled)
			{
			}
			else
			{
				if (!other.gameObject.GetComponent<doorTriggerScript>().DoorTriggered)
				{
					other.gameObject.GetComponent<doorTriggerScript>().DoorTriggered = true;
					key1.GetComponent<KeyScript>().KeyEnabled = false;
					LetGoKey();
					other.gameObject.SetActive(false);
					print($"setting {other.gameObject}.SetActive to false");
				}
			}
		}
		//else if (other.gameObject.name == "AtomBall(Clone)") {
		else if (other.gameObject.name == "DoorKey(Clone)")
		{
			Debug.Log("Touching DoorKey");
			if (!key1.GetComponent<KeyScript>().KeyEnabled)
			{
				key1.GetComponent<KeyScript>().KeyEnabled = true;
				other.gameObject.SetActive(false);
			}
		}
		else if (other.gameObject.tag == "Arrow")
		{
			Debug.Log("Touching Arrow");
			other.gameObject.GetComponent<ArrowTagTrigger>().Touched();

		}
		else
		//Debug.Log("what did I touch ? : " + other.gameObject.name + " with a tag " + other.gameObject.tag);
		{ }
	}

	public void PickUpKey ()
	{
		GetComponent<Animation>().Play ("Attack_01");
		keysInHand.ForEach(k => k.SetActive (true));
	}

	private void LetGoKey ()
	{
		keysInHand.ForEach(k => k.SetActive(false));
	}
}

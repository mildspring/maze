﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelFinished : MonoBehaviour {
	public GameObject teleporter;
	public float endScale;
	public float speed;
	private Vector3 originalLocalScale;
	private Vector3 finalLocalScale;

	public AudioClip levelFinishedSound;
	public AudioClip levelFinishedSound2;

	public GameObject EventHandlerObject;
	
	// Use this for initialization
	void Start () {
		Debug.Log ("LevelFinished started");

		int nextHighestLevel = VarEnums.GetInt (VarEnums.NextHighestLevel, 1);
		int NextLevel = VarEnums.GetInt (VarEnums.NextLevel, 1);

		EventHandlerObject.GetComponent<EventHandler> ().TriggerEvent (EventHandlerValues.LEVEL_FINISHED_SUCCESSFULLY, NextLevel);

		if (NextLevel == nextHighestLevel) {
			VarEnums.SetInt(VarEnums.NextHighestLevel, nextHighestLevel+1);
			int PathOwned = VarEnums.GetInt(VarEnums.PathOwned);
			VarEnums.SetInt(VarEnums.PathOwned, PathOwned + 1);

			EventHandlerObject.GetComponent<EventHandler> ().TriggerEvent (EventHandlerValues.LEVEL_NEW_UNLOCKED, nextHighestLevel+1);
		}

		VarEnums.SetInt(VarEnums.GameInProgress, 0);

		GetComponent<Utility_MusicQueue> ().Add (levelFinishedSound);
		GetComponent<Utility_MusicQueue> ().Add (levelFinishedSound2);
		GetComponent<Utility_MusicQueue> ().Play ();

		Celebrate ();
	}

	void Celebrate () {
		GameObject Light = GameObject.Find("Point light");
		GameObject Camera = GameObject.Find("Camera");
		Debug.Log ("Test is pressed camera.position:" + Camera.transform.localPosition + " localRotation: " + Camera.transform.localRotation);
		//Camera.transform.position = new Vector3(Camera.transform.position.x, Camera.transform.position.y, Camera.transform.position.z * -1);
		
		Camera.transform.localPosition = new Vector3(Camera.transform.localPosition.x, Camera.transform.localPosition.y, Camera.transform.localPosition.z * -1);
		Light.transform.localPosition = new Vector3(Light.transform.localPosition.x, Light.transform.localPosition.y, Light.transform.localPosition.z * -1);
		//Camera.transform.position = new Vector3(Camera.transform.position.x, Camera.transform.position.y * -1, Camera.transform.position.z);
		
		//Camera.transform.localRotation = new Vector3(Camera.transform.localRotation.x, Camera.transform.localRotation.y + 180, Camera.transform.localRotation.z);
		//Camera.transform.rotation = new Vector3(Camera.transform.rotation.x, Camera.transform.rotation.y + 180, Camera.transform.rotation.z);
		Camera.transform.Rotate(Vector3.up * 180);
		//Light.transform.Rotate(Vector3.up * 180);
		//player.transform.rotation = Quaternion.Euler(0,Camera.main.transform.eulerAngles.y - 180,0);
		
		GameObject Troll = GameObject.Find ("Troll");
		if (Troll == null) {
			Debug.Log ("Troll is null");
		}
		else {
			//Troll.component
			//PlayerScript TheScript = Troll.GetComponent<PlayerScript>();
			//PlayerScript TheScript = Troll.GetComponentInChildren<PlayerScript>();
			PlayerScript TheScript = Troll.GetComponentInParent<PlayerScript>();
			//PlayerScript TheScript = Troll.GetComponent("PlayerScript") as PlayerScript;
			if (TheScript == null) {
				Debug.Log ("TheScript is null");
			}
			else {
				TheScript.Celebrate(CelebrationOver);
			}
		}
	}
	
	void CelebrationOver() {
		//Application.LoadLevel("PickLevel");
		SceneManager.LoadScene("PickLevel");
	}
}

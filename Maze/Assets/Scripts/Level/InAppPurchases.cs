﻿using UnityEngine;
using OnePF;
using System.Collections.Generic;

public class InAppPurchases : MonoBehaviour {
	const string SKU1 = "path3";

	const string SKU2 = "door1";

	#pragma warning disable 0414
	string _label = "";
	#pragma warning restore 0414
	
	bool _isInitialized = false;
	
	private void OnEnable() {
		// Listen to all events for illustration purposes
		OpenIABEventManager.billingSupportedEvent += billingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent += purchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
	}
	private void OnDisable() {
		// Remove all event handlers
		OpenIABEventManager.billingSupportedEvent -= billingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
	}
	
	private void Start() {
		// Map skus for different stores
		OpenIAB.mapSku(SKU1, OpenIAB_Android.STORE_GOOGLE, "path3");
		OpenIAB.mapSku(SKU1, OpenIAB_Android.STORE_AMAZON, "path3");
	
		// Application public key
		// from sample: var public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqibEPHCtfPm3Rn26gbE6vhCc1d6A072im+oWNlkUAJYV//pt1vCkYLqkkw/P2esPSWaw1nt66650vfVYc3sYY6L782n/C+IvZWQt0EaLrqsSoNfN5VqPhPeGf3wqsOvbKw9YqZWyKL4ddZUzRUPex5xIzjHHm3qIJI5v7iFJHOxOj0bLuEG8lH0Ljt/w2bNe4o0XXoshYDqpzIKmKy6OYNQOs8iBTJlfSmPrlGudmldW6CsuAKeVGm+Z+2xx3Xxsx3eSwEgEaUc1ZsMWSGsV6dXgc3JrUvK23JRJUu8X5Ec1OQLyxL3VelD5f0iKVTJ1kw59tMAVZ7DDpzPggWpUkwIDAQAB";
		var public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiMyk8RL29fr6BNpTv25y5O22KJQwpr5D/Sf5GCxjiPItEYZ0lsTpl4UIVCpHu2HD8VcjqYyI2GZUZqHIVdk07A4q71mQQjoVODSjtpXHNFMf0A7tQUs6JfDkp9jmOTkEyH93OTqBfPVDFYFIXYfMsdfPpBdfNA6S9yRdpBC//0MUZz8mGxopiBy1GZnSbBJs29QAbQg5VoNSPQWZD0g4D2ke5YZC6GZc2U1KT6bUGkVe15R6s46n9QSoJfzPa1AAB0xzpVN5u5Z2nwxxq4M6dIaPimOhOlaic7rNxbvpqm/lfpnD5GLgGye7f+92icZqli51O0pwRRjreVEf0f5IHQIDAQAB";
		var options = new Options();
		options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;
		options.storeKeys = new Dictionary<string, string> {
			{OpenIAB_Android.STORE_GOOGLE, public_key}
		};
		
		// Transmit options and start the service
		OpenIAB.init(options);
	}
	
	#if UNITY_ANDROID
	private void OnGUI_NOTUSED() {
		float yPos = 5.0f;
		float xPos = 5.0f;
		float width = (Screen.width >= 800 || Screen.height >= 800) ? 320 : 160;
		float height = (Screen.width >= 800 || Screen.height >= 800) ? 80 : 40;
		float heightPlus = height + 10.0f;
		
		if (GUI.Button(new Rect(xPos, yPos, width, height), "Initialize OpenIAB")) {
			//InitPurchases();
		}
		
		if (!_isInitialized)
			return;
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Test Purchase")) {
			OpenIAB.purchaseProduct("android.test.purchased");
		}
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Test Refund")) {
			OpenIAB.purchaseProduct("android.test.refunded");
		}
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Test Item Unavailable")) {
			OpenIAB.purchaseProduct("android.test.item_unavailable");
		}
		
		xPos = Screen.width - width - 5.0f;
		yPos = 5.0f;
		
		if (GUI.Button(new Rect(xPos, yPos, width, height), "Test Purchase Canceled")) {
			OpenIAB.purchaseProduct("android.test.canceled");
		}
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Query Inventory")) {
			OpenIAB.queryInventory(new string[] { SKU1, SKU2 });
		}
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Purchase Real Product")) {
			OpenIAB.purchaseProduct(SKU1);
		}
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Consume Real Product")) {
			OpenIAB.consumeProduct(Purchase.CreateFromSku(SKU1));
		}

		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Purchase Real Product2")) {
			OpenIAB.purchaseProduct(SKU2);
		}
		
		if (GUI.Button(new Rect(xPos, yPos += heightPlus, width, height), "Consume Real Product2")) {
			OpenIAB.consumeProduct(Purchase.CreateFromSku(SKU2));
		}

		GUIStyle GUIStyle = new GUIStyle();
		GUIStyle.fontSize = 20;
		GUI.Label(new Rect(10, 20 + Screen.height * 0.1f, Screen.width, Screen.height), _label, GUIStyle);

	}
	#endif
	
	#if UNITY_WP8
	void OnGUI()
	{
		if (GUI.Button(new Rect(10, 10, Screen.width * 0.3f, Screen.height * 0.1f), "QUERY INVENTORY"))
		{
			OpenIAB.queryInventory(new string[] { SKU_AMMO, SKU_MEDKIT, SKU_SUBSCRIPTION });
		}
		if (GUI.Button(new Rect(20 + Screen.width * 0.3f, 10, Screen.width * 0.3f, Screen.height * 0.1f), "Purchase"))
		{
			OpenIAB.purchaseProduct(SKU_MEDKIT);
		}
		if (GUI.Button(new Rect(30 + Screen.width * 0.6f, 10, Screen.width * 0.3f, Screen.height * 0.1f), "Consume"))
		{
			OpenIAB.consumeProduct(Purchase.CreateFromSku(SKU_MEDKIT));
		}
		GUI.Label(new Rect(10, 20 + Screen.height * 0.1f, Screen.width, Screen.height), _label);
	}
	#endif

	public void purchaseProduct() {
		OpenIAB.purchaseProduct(SKU1);
	}

//	public void consumeProduct() {
//		OpenIAB.consumeProduct(Purchase.CreateFromSku(SKU1));
//	}

	public void purchaseTestProduct() {
		OpenIAB.purchaseProduct("android.test.purchased");
	}
	
//	public void consumeTestProduct() {
//		OpenIAB.consumeProduct(Purchase.CreateFromSku("android.test.purchased"));
//	}


	private void billingSupportedEvent() {
		_isInitialized = true;
		Debug.Log("billingSupportedEvent");
	}
	private void billingNotSupportedEvent(string error) {
		Debug.Log("billingNotSupportedEvent: " + error);
	}
	private void queryInventorySucceededEvent(Inventory inventory) {
		Debug.Log("queryInventorySucceededEvent: " + inventory);
		if (inventory != null)
			_label = inventory.ToString();
	}
	private void queryInventoryFailedEvent(string error) {
		Debug.Log("queryInventoryFailedEvent: " + error);
		_label = error;
	}
	private void purchaseSucceededEvent(Purchase purchase) {
		Debug.Log("purchaseSucceededEvent: " + purchase + " SKU: " + purchase.Sku);
		_label = "PURCHASED:" + purchase.ToString();
		if (purchase.Sku == SKU1) {
			int pathOwned = Mathf.Max(VarEnums.GetInt (VarEnums.PathOwned, 1), 0) + 3;
			VarEnums.SetInt (VarEnums.PathOwned, pathOwned);
			GetComponent<SceneScript>().SetPathOwned(pathOwned);
			OpenIAB.consumeProduct(purchase);
		}
	}
	private void purchaseFailedEvent(int errorCode, string errorMessage) {
		Debug.Log("purchaseFailedEvent: " + errorMessage);
		_label = "Purchase Failed: " + errorMessage;
	}
	private void consumePurchaseSucceededEvent(Purchase purchase) {
		Debug.Log("consumePurchaseSucceededEvent: " + purchase);
		_label = "CONSUMED: " + purchase.ToString();
	}
	private void consumePurchaseFailedEvent(string error) {
		Debug.Log("consumePurchaseFailedEvent: " + error);
		_label = "Consume Failed: " + error;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelInfo : MonoBehaviour {
	private List<Wall> wallsLeft;
	private List<DoorPosition> doorPositions = new List<DoorPosition> ();
	private Position finalPosition = null;
	private int gridX;
	private int gridY;
	private GameObject playerObject;

	public List<Wall> WallsLeft {
		get {
			return wallsLeft;
		}
		set {
			wallsLeft = value;
		}
	}
	public List<DoorPosition> DoorPositions {
		get {
			return doorPositions;
		}
		set {
			doorPositions = value;
		}
	}
	public Position FinalPosition {
		get {
			return finalPosition;
		}
		set {
			finalPosition = value;
		}
	}
	public int GridX {
		get {
			return gridX;
		}
		set {
			gridX = value;
		}
	}
	public int GridY {
		get {
			return gridY;
		}
		set {
			gridY = value;
		}
	}
	public GameObject PlayerObject {
		get {
			return playerObject;
		}
		set {
			playerObject = value;
		}
	}
}

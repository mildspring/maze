﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PossiblePath {
	internal int estimatedLeftSteps;
	internal List<Position> path;

	internal PossiblePath(int EstimatedLeftSteps, List<Position> Path) {
		estimatedLeftSteps = EstimatedLeftSteps;
		path = Path;
	}

	public override string ToString ()
	{
		return string.Format ("[PathFinder] estimatedLeftSteps " + estimatedLeftSteps + " path " + path);
	}
}

public class PathFinder {

	int x;
	int y;
	List <PossiblePath> paths;
	List<Wall> wallsLeft;
	Dictionary<Position, bool> processedPositions;
	List<Position> foundPath;
	Position from;
	Position to;

	List<PossiblePath> allPaths = new List<PossiblePath>();

	public List<PossiblePath> AllPaths { get { return allPaths; } }

	/// <summary>
	/// Initializes a new instance of the <see cref="PathFinder"/> class.
	/// </summary>
	/// <param name="Positions">Positions.</param>
	public PathFinder(int X, int Y, List<Wall> WallsLeft) {
		x = X;
		y = Y;
		wallsLeft = WallsLeft;
	}

	public List<Position> PositionsInAllPaths() {
		List<Position> AllPositions = new List<Position> ();
		foreach (PossiblePath PossiblePath in allPaths) {
			foreach (Position Position in PossiblePath.path) {
				if (!AllPositions.Contains(Position)) {
					AllPositions.Add (Position);
				}
			}
		}
		return AllPositions;
	}

	/// <summary>
	/// Processes the new position by adding it to paths if it's allowed, and possibly setting foundPath.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="tail">Tail.</param>
	private void processNewPosition(int startingX, int startingY, int x, int y, List<Position> tail) {
		Position NewPosition = new Position(x, y);
		Wall WallBetween = new Wall (new Position (startingX, startingY), new Position (x, y));
		//Debug.Log ("nx=" + newPosition.x + " x=" + this.x + " ny=" + newPosition.y + " y=" + this.y +
		//		" process_contains=" + processedPositions.ContainsKey (newPosition) + " walls_contains=" + wallsLeft.Contains (wallBetween));
		if (NewPosition.x < this.x && NewPosition.y < this.y && NewPosition.x >= 0 && NewPosition.y >= 0 &&
		    	!processedPositions.ContainsKey (NewPosition) && !wallsLeft.Contains(WallBetween)) {
			List<Position> CurrentCost = new List<Position>();

			CurrentCost.Add (NewPosition);
			if (tail != null) {
				CurrentCost.AddRange(tail);
			}
			PossiblePath newPossiblePath = new PossiblePath(Mathf.Abs(x - to.x) + Mathf.Abs(y - to.y), CurrentCost);
			paths.Add (newPossiblePath);
			//Debug.Log ("Adding path: " + newPossiblePath.ToString());

			if (newPossiblePath.estimatedLeftSteps == 0) {
				foundPath = newPossiblePath.path;
			}
		}
	}

	/// <summary>
	/// Finds the path.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="From">From.</param>
	/// <param name="To">To.</param>
	public List<Position> findPath(Position From, Position To, bool StopOnFindPath) {
		from = From;
		to = To;
		foundPath = null;

		paths = new List<PossiblePath> ();

		processedPositions = new Dictionary<Position, bool>();


//		Debug.Log ("finding from=" + from + " to=" + to);
		processNewPosition (from.x, from.y, from.x, from.y, null);
		//Debug.Log ("after first processNewPosition");
		//Debug.Log ("processed: " + from);
		//processedPositions.Add (from, true);

		//Debug.Log ("outside paths.Count is " + paths.Count);
		while ((!StopOnFindPath || foundPath == null) && paths.Count > 0) {
			//Debug.Log ("paths.Count is " + paths.Count);
			PossiblePath currentPosition = paths[0];
			paths.Remove(currentPosition);

			if (!processedPositions.ContainsKey(currentPosition.path[0])) {
				int PathsCount = paths.Count;

				processNewPosition (currentPosition.path[0].x, currentPosition.path[0].y, currentPosition.path[0].x-1, currentPosition.path[0].y, currentPosition.path);
				processNewPosition (currentPosition.path[0].x, currentPosition.path[0].y, currentPosition.path[0].x+1, currentPosition.path[0].y, currentPosition.path);
				processNewPosition (currentPosition.path[0].x, currentPosition.path[0].y, currentPosition.path[0].x, currentPosition.path[0].y+1, currentPosition.path);
				processNewPosition (currentPosition.path[0].x, currentPosition.path[0].y, currentPosition.path[0].x, currentPosition.path[0].y-1, currentPosition.path);

				if (paths.Count == PathsCount) {
					allPaths.Add(currentPosition);
				}

				//Debug.Log ("processed: " + currentPosition.path[0]);
				processedPositions.Add (currentPosition.path[0], true);

				// sort positions by length
				paths.Sort(delegate(PossiblePath one, PossiblePath two)
				           {
					return (one.estimatedLeftSteps + one.path.Count) - (two.estimatedLeftSteps + two.path.Count);
				});
			}
		}

		return foundPath;
	}

	public void DebugPrintFoundPath() {
		foreach (Position position in foundPath) {
			Debug.Log(position);
		}
	}
}

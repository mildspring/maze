﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SceneScript : MonoBehaviour
{

	public Texture OnGuiConfig;
	public GameObject wall;
	public float wallScaleX;
	public float wallScaleY;
	public float wallScaleZ;
	public GameObject player;
	public GameObject lastPosition;
	public GameObject halloweenLastPosition;
	public GameObject door;
	public GameObject doorTrigger;
	public int numberOfDoors;
	public bool doorsRequireKey;
	public GameObject key;
	public GameObject arrowToExit;
	public Texture rightArrow;
	public Texture leftArrow;
	public Texture runningIcon;
	public Texture stoppedIcon;
	//public Texture keyIcon;
	private ButtonPosition buttonPosition = ButtonPosition.right; // no longer configurable from inspector

	Position playerPosition = null;
	private bool possibleExit = false;
	List<Position> thePath = null;
	private List<GameObjectPosition>PathArrows = new List<GameObjectPosition> ();
	private bool refreshPath;
	private bool displayPathIcon;
    private readonly bool allowingDisplayPathIcon = false;
    public Texture pathIcon;
	private int pathOwned;

	//public Texture pathConfirmMenu;
	private bool displayPathBuyConfirmMenu = false;
	private bool displayPathUseConfirmMenu = false;
	private Utility_Menu Utility_Menu;
	public GameObject key1;
	private PlayerScript playerScript;
	private LevelInfo levelInfo;
	private static bool firstLoadingFrame = true;
	private Vector3 vector3PlayerPosition = new Vector3 ();
	private Vector3 vector3PlayerRotation = new Vector3 ();
	private GameObject[] minimapObjects = null;

	// Use this for initialization
	private void Awake ()
	{
		firstLoadingFrame = true;
		minimapObjects = GameObject.FindGameObjectsWithTag ("MiniMapCamera");
		if (minimapObjects != null)
		{
			foreach (GameObject MiniMapCamera in minimapObjects)
			{
				MiniMapCamera.SetActive (false);
			}
		}
		LoadLevel ();
	}

	private void LoadLevel ()
	{
		levelInfo = GetComponent<LevelInfo> ();
		levelInfo.PlayerObject = player;
		int SecondsElapsed = 0;
		// section that works for both loaded and not loaded levels
		displayPathIcon = (VarEnums.GetInt (VarEnums.NextLevel, 1) > 1);
		pathOwned = Mathf.Max (VarEnums.GetInt (VarEnums.PathOwned, 1), 0);

		if (VarEnums.GetInt (VarEnums.Width, -1) != -1)
		{
			levelInfo.GridX = VarEnums.GetInt (VarEnums.Width, -1);
		}
		else
		{
			levelInfo.GridX = 5;
		}
		if (VarEnums.GetInt (VarEnums.Height, -1) != -1)
		{
			levelInfo.GridY = VarEnums.GetInt (VarEnums.Height, -1);
		}
		else
		{
			levelInfo.GridY = 5;
		}
		if (VarEnums.GetInt (VarEnums.Doors, -1) != -1)
		{
			numberOfDoors = VarEnums.GetInt (VarEnums.Doors, -1);
		}

		bool LoadedLevel = false;
		if (VarEnums.GetInt (VarEnums.GameInProgress, 0) == 1)
		{
			VarEnums.SetInt (VarEnums.GameInProgress, 0);
			// loading level
			bool SaveFileLoadedSuccessfully = false;
			try
			{
				SaveFileLoadedSuccessfully = SaveLoad.Load ();
			}
			catch (System.Exception Ex)
			{
				Debug.Log ("Failed to load level: " + Ex.Message);
			}
			if (SaveFileLoadedSuccessfully == true)
			{
				GameState CurrentGameState = new GameState ();
				if (GameState.current.structureVersion == CurrentGameState.structureVersion)
				{
					Debug.Log ("Loaded level: " + GameState.current);

					vector3PlayerPosition = new Vector3 (((float)GameState.current.playerPosition.x) / 1000, this.player.transform.position.y, (float)GameState.current.playerPosition.y / 1000);
					vector3PlayerRotation = new Vector3 (0.0f, GameState.current.playerRotation, 0.0f);
					levelInfo.WallsLeft = GameState.current.wallsLeft;
					levelInfo.DoorPositions = DoorPositionSerializable.ToDoorPosition (GameState.current.doorPositions);
					levelInfo.FinalPosition = GameState.current.finalPosition;
					if (Utility_CurrentTheme.CurrentTheme == Themes.Halloween)
					{
						halloweenLastPosition.transform.position = new Vector3 (levelInfo.FinalPosition.x, 0.0f, levelInfo.FinalPosition.y + 0.5f);
					}
					else
					{
						//try
						{
							// getting null, hopefully only in debugger.
							//Debug.Log ("got issue: FinalPosition " + ((levelInfo.FinalPosition == null) ? " ** NULL ** " : levelInfo.FinalPosition.ToString()));
							//							
							lastPosition.transform.position = new Vector3 (levelInfo.FinalPosition.x, 0.0f, levelInfo.FinalPosition.y + 0.5f);
						}
//						catch (System.Exception Ex)
//						{
//							Debug.Log ("got issue: FinalPosition " + levelInfo.FinalPosition);
//							throw;
//						}
					}
					key1.GetComponent<KeyScript> ().KeyEnabled = GameState.current.haveKey;
					PathArrows = GameState.current.pathArrows;
					SecondsElapsed = GameState.current.timeElapsed;

					Position target = levelInfo.FinalPosition;
					for (int i = 0; i < PathArrows.Count; ++i)
					{
						GameObject GameObject2 = (GameObject)Instantiate (arrowToExit, new Vector3 (PathArrows [i].position.x, 0.0f, PathArrows [i].position.y + 0.5f), Quaternion.identity);
						GameObject2.transform.Rotate (0, 90, 0);
						if (PathArrows [i].position.x == target.x)
						{
							if (PathArrows[i].position.y < target.y)
							{ }
							else
								GameObject2.transform.Rotate(0, 180, 0);
						}
						else if (PathArrows [i].position.x > target.x)
							GameObject2.transform.Rotate (0, 270, 0);
						else
							GameObject2.transform.Rotate (0, 90, 0);
							
							
						PathArrows [i].gameObject = GameObject2;
						target = PathArrows [i].position;
					}

					LoadedLevel = true;
				}
				else
				{
					Debug.Log ("Unable to process Save Game file version: " + GameState.current.structureVersion);
				}

			}
			else
			{
				Debug.Log ("Could not load level.");
			}
		}

		if (!LoadedLevel)
		{
			//Debug.Log ("Did not load level, because it was not in progress");
		

			Debug.Log ("Creating Grid " + levelInfo.GridX + " by " + levelInfo.GridY + " with " + numberOfDoors + " doors");
			Grid Grid = new Grid (levelInfo.GridX, levelInfo.GridY);

			//Debug.Log (Grid);

			List<Position> Balls = new List<Position> ();

			while (thePath == null)
			{
				playerPosition = new Position (Mathf.RoundToInt (Random.Range (1, levelInfo.GridX)), Mathf.RoundToInt (Random.Range (1, levelInfo.GridY)));
				levelInfo.FinalPosition = new Position (Mathf.RoundToInt (Random.Range (1, levelInfo.GridX)), Mathf.RoundToInt (Random.Range (1, levelInfo.GridY)));

				if (Utility_CurrentTheme.CurrentTheme == Themes.Halloween)
				{
					halloweenLastPosition.transform.position = new Vector3 (levelInfo.FinalPosition.x, 0.0f, levelInfo.FinalPosition.y + 0.5f);
				}
				else
				{
					lastPosition.transform.position = new Vector3 (levelInfo.FinalPosition.x, 0.0f, levelInfo.FinalPosition.y + 0.5f);
				}

				PathFinder pathFinder = new PathFinder (levelInfo.GridX, levelInfo.GridY, Grid.WallsLeft);
				thePath = pathFinder.findPath (playerPosition, levelInfo.FinalPosition, false);
				if (thePath == null)
				{
					//Debug.Log ("Could not find path :(");
				}
				else if (thePath.Count < levelInfo.GridX + levelInfo.GridY)
				{
					//Debug.Log ("thePath.Count " + thePath.Count + " is way too small");
					thePath = null;
				}
				else if (thePath.Count / (numberOfDoors + 1) < 4)
				{
					//Debug.Log ("path not long enough for doors");
					thePath = null;
				}
				else
				{
					//Debug.Log ("pathFinder returned " + thePath + " which has " + thePath.Count + " elements");
					//pathFinder.DebugPrintFoundPath ();
				}
			}

//			player.transform.position = new Vector3 (playerPosition.x, 0.0f, playerPosition.y + 0.5f);
//			player.transform.Rotate (Vector3.down * Random.Range (0, 360));
			vector3PlayerPosition = new Vector3 (playerPosition.x, 0.0f, playerPosition.y + 0.5f);
			vector3PlayerRotation = Vector3.down * Random.Range (0, 360);


			levelInfo.WallsLeft = Grid.WallsLeft;


			// calculate where to put doors
			if (numberOfDoors > 0)
			{
				int RangeOfPositionsPerDoor = thePath.Count / (numberOfDoors + 1);

				for (int CurrentDoor = numberOfDoors-1; CurrentDoor >= 0; --CurrentDoor)
				{

					//int doorLocationInPath = Random.Range (2, RangeOfPositionsPerDoor-3) + RangeOfPositionsPerDoor * CurrentDoor;
					int doorLocationInPath = RangeOfPositionsPerDoor * (1 + CurrentDoor);
					//Debug.Log ("doorLocationInPath is " + doorLocationInPath + " thepath.Count is " + thePath.Count);
//					Wall doorWall = new Wall(thePath [doorLocationInPath], thePath[doorLocationInPath+1]);
//
//					GameObject theDoor = InstantiateWall(door, doorWall);
//					GameObject theDoorTrigger1 = (GameObject) Instantiate(doorTrigger, new Vector3(doorWall.One.x, 0, doorWall.One.y+0.5f), Quaternion.identity);
//					GameObject theDoorTrigger2 = (GameObject) Instantiate(doorTrigger, new Vector3(doorWall.Two.x, 0, doorWall.Two.y+0.5f), Quaternion.identity);
//					theDoor.GetComponent<doorScript> ().NeedsKey = this.doorsRequireKey;
//					theDoorTrigger1.GetComponent<doorTriggerScript> ().Door = theDoor;
//					theDoorTrigger2.GetComponent<doorTriggerScript> ().Door = theDoor;

					DoorPosition doorPosition = new DoorPosition (thePath [doorLocationInPath + 1], thePath [doorLocationInPath]);
//					DoorPosition.door = theDoor;
					levelInfo.DoorPositions.Add (doorPosition);
					Debug.Log($"Added door: {doorPosition}");
				}


				if (this.doorsRequireKey)
				{
					Position StartingPosition = playerPosition;
					for (int DoorIndex = 0; DoorIndex < levelInfo.DoorPositions.Count; ++DoorIndex)
					{
						Position TargetPosition;
						List<Wall> WallListToDoor = Wall.CopyList (levelInfo.WallsLeft);

						//Debug.Log ("Processing door: " + levelInfo.DoorPositions[DoorIndex]);

						TargetPosition = levelInfo.DoorPositions [DoorIndex].closerToPlayer;

						if (DoorIndex > 0)
						{
							WallListToDoor.Add (new Wall (levelInfo.DoorPositions [DoorIndex - 1].closerToPlayer, levelInfo.DoorPositions [DoorIndex - 1].closerToFinalPosition));
						}
						WallListToDoor.Add (new Wall (levelInfo.DoorPositions [DoorIndex].closerToPlayer, levelInfo.DoorPositions [DoorIndex].closerToFinalPosition));

						PathFinder PathToDoor = new PathFinder (levelInfo.GridX, levelInfo.GridY, WallListToDoor);
						PathToDoor.findPath (StartingPosition, TargetPosition, false);
						List <Position> PossibleKeyPositions = PathToDoor.PositionsInAllPaths ();
//						Debug.Log ("original PossibleKeyPositions.Count = " + PossibleKeyPositions.Count);
						PossibleKeyPositions.Remove (StartingPosition);
						PossibleKeyPositions.Remove (TargetPosition);

						StartingPosition = levelInfo.DoorPositions [DoorIndex].closerToFinalPosition;

//						Debug.Log ("after removing2 PossibleKeyPositions.Count = " + PossibleKeyPositions.Count);

						Position ballPosition = PossibleKeyPositions [Mathf.RoundToInt (Random.Range (0, PossibleKeyPositions.Count - 1))];
//						Debug.Log ("Calculated ball position: " + BallPosition + " player position: " + playerPosition);
						Balls.Add (ballPosition);

						levelInfo.DoorPositions[DoorIndex].keyPosition = ballPosition;
						Debug.Log($"Added key: {ballPosition}");
					}
				}
			}
		}

		// DRAWING SECTION ---- 

		foreach (DoorPosition Door in levelInfo.DoorPositions)
		{
			Position Ball = Door.keyPosition;
			GameObject GameObject = (GameObject)Instantiate (key, new Vector3 (Ball.x + 0.4f, 0.1f, ((float)Ball.y) + 0.5f), Quaternion.identity);
			GameObject.transform.Rotate (new Vector3 (270, 45, 0));
			Door.key = GameObject;

			Wall doorWall = new Wall (Door.closerToFinalPosition, Door.closerToPlayer);
			
			GameObject theDoor = InstantiateWall (door, doorWall);
			//GameObject theDoorTrigger1 = (GameObject)Instantiate (doorTrigger, new Vector3 (doorWall.One.x, 0, doorWall.One.y + 0.5f), Quaternion.identity);
			//GameObject theDoorTrigger2 = (GameObject)Instantiate (doorTrigger, new Vector3 (doorWall.Two.x, 0, doorWall.Two.y + 0.5f), Quaternion.identity);
			theDoor.GetComponent<doorScript> ().NeedsKey = this.doorsRequireKey;
			//theDoorTrigger1.GetComponent<doorTriggerScript> ().Door = theDoor;
			//theDoorTrigger2.GetComponent<doorTriggerScript> ().Door = theDoor;
			//GameObject theDoorTrigger = (GameObject)Instantiate(doorTrigger, new Vector3((doorWall.One.x + doorWall.Two.x) / 2, 0, doorWall.Two.y + 0.0f), Quaternion.identity);
			GameObject theDoorTrigger = (GameObject)Instantiate(doorTrigger, theDoor.transform.position, Quaternion.identity);
			theDoorTrigger.GetComponent<doorTriggerScript>().Door = theDoor;
			Door.door = theDoor;
		}

		//System.TimeSpan MyTimeSpan = new System.TimeSpan ();
		int MyLoops = 0;
		foreach (Wall theWall in levelInfo.WallsLeft)
		{
			InstantiateWall (wall, theWall);
			//MyTimeSpan.Add (System.DateTime.Now - StartTime);
			++MyLoops;
		}

		// draw outline
		for (int x = 0; x < levelInfo.GridX; ++x)
		{
			InstantiateWall (wall, x, 0, false);
			InstantiateWall (wall, x, levelInfo.GridY, false);
			//MyTimeSpan.Add (System.DateTime.Now - StartTime);
			++MyLoops;
		}
		//MyTimeSpans.Add (new MyTimeSpan ("O1-" + MyLoops, MyTimeSpan));
		//MyTimeSpan = new System.TimeSpan ();
		for (int y = 0; y < levelInfo.GridY; ++y)
		{
			//yield return null;
			InstantiateWall (wall, -0.5f, y + 0.5f, true);
			InstantiateWall (wall, levelInfo.GridX + -0.5f, y + 0.5f, true);
			//MyTimeSpan.Add (System.DateTime.Now - StartTime);
			++MyLoops;
		}

		if (LoadedLevel)
		{
			DoorPosition.NormalizeDoorsFromLevel (levelInfo.DoorPositions);
		}

		VarEnums.SetInt (VarEnums.GameInProgress, 1);
		Utility_LevelTimer.StartLevel ();
		Utility_LevelTimer.SetTimePassed (SecondsElapsed);
	}

	void Start ()
	{
		Utility_Menu = GetComponent<Utility_Menu> ();
		playerScript = player.GetComponent<PlayerScript> ();
	}

	public void SetPathOwned (int PathOwned)
	{
		pathOwned = PathOwned;
	}
	
	void OnGUI ()
	{
		if (SceneScript.firstLoadingFrame)
		{
			if (minimapObjects != null)
			{
				foreach (GameObject MiniMapCamera in minimapObjects)
				{
					MiniMapCamera.SetActive (true);
				}
			}
			player.transform.position = vector3PlayerPosition;
			player.transform.Rotate (vector3PlayerRotation);
			firstLoadingFrame = false;
		}

		float OffsetUI = Mathf.Min (Screen.height, Screen.width) / 30;

		if (displayPathBuyConfirmMenu)
		{
//			playerScript.WalkButtonToggled = false;
//			float XOffset = Screen.width / 2 - (Screen.height - OffsetUI * 2) / 2;
//			GUI.DrawTexture(new Rect(XOffset, OffsetUI, Screen.height - OffsetUI * 2, Screen.height - OffsetUI * 2), pathConfirmMenu);
			Utility_Menu.Menu ("pathBuy", "Would you like to buy\na pack of 3 paths?", MenuOK, MenuCancel);
			return;
		}
		if (displayPathUseConfirmMenu)
		{
			Utility_Menu.Menu ("pathUse", "Would you like to use\none path?", MenuOK, MenuCancel);
			return;
		}
		if (possibleExit)
		{
			Utility_Menu.Menu ("exitPrompt", "Exit to menu,\nand lose progress?", this.MenuOK, this.MenuCancel);

			return;
		}

		if (buttonPosition == ButtonPosition.right)
		{
			if (Utility_OnGUI.Button (new Rect (OffsetUI, OffsetUI, OffsetUI * 4, OffsetUI * 4), OnGuiConfig))
			{
				playerScript.WalkButtonToggled = false;
				possibleExit = true;
			}

			if (displayPathIcon && allowingDisplayPathIcon)
			{
				Rect PathIconPosition = new Rect (Screen.width - OffsetUI * 7, Screen.height - (OffsetUI * 7) * 3, OffsetUI * 6, OffsetUI * 6);
				if (Utility_OnGUI.Button (PathIconPosition, pathIcon))
				{
					playerScript.WalkButtonToggled = false;
					if (pathOwned > 0)
					{
						displayPathUseConfirmMenu = true;
					}
					else
					{
						displayPathBuyConfirmMenu = true;
					}
				}
				GUIStyle GUIStyle = new GUIStyle ();
				GUIStyle.fontSize = Screen.height / 14;
				GUIStyle.normal.textColor = Color.blue;
				GUIStyle.fontStyle = FontStyle.BoldAndItalic;
				GUI.Label (new Rect (Screen.width - OffsetUI * 5, Screen.height - (OffsetUI * 7) * 3 + (OffsetUI * 3), OffsetUI * 6, OffsetUI * 6), pathOwned.ToString (), GUIStyle);
				//GUI.Label(new Rect(10, 20 + Screen.height * 0.1f, Screen.width, Screen.height), pathOwned.ToString(), GUIStyle);
			}

			// direction buttons

			if (Utility_OnGUI.RepeatButton (new Rect (Screen.width - OffsetUI * 7, Screen.height - OffsetUI * 7, OffsetUI * 6, OffsetUI * 6), rightArrow))
			{
				playerScript.RightButtonHeld = true;
			}
			
			if (Utility_OnGUI.RepeatButton (new Rect (Screen.width - OffsetUI * 7 - OffsetUI * 8, Screen.height - OffsetUI * 7, OffsetUI * 6, OffsetUI * 6), leftArrow))
			{
				playerScript.LeftButtonHeld = true;
			}
			
			//if (Utility_OnGUI.Button (new Rect (Screen.width - OffsetUI * 7 - OffsetUI * 15, Screen.height - OffsetUI * 7, OffsetUI * 6, OffsetUI * 6), playerScript.WalkButtonToggled ? stoppedIcon : runningIcon))
			if (Utility_OnGUI.Button (new Rect (OffsetUI, Screen.height - OffsetUI * 7, OffsetUI * 6, OffsetUI * 6), playerScript.WalkButtonToggled ? stoppedIcon : runningIcon))
			{
				playerScript.WalkButtonToggled = !playerScript.WalkButtonToggled;
			}

/*			if (key1.GetComponent<KeyScript>().KeyEnabled)
            {
				// adding key button
				Rect rect = new();
				rect.x = Screen.width - OffsetUI * 7 - OffsetUI * 8;
				rect.y = OffsetUI;
				rect.width = OffsetUI * 4;
				rect.height = OffsetUI * 4;
				Utility_OnGUI.Button(rect, keyIcon);
			}*/
		}

		//if (Utility_OnGUI.Button (new Rect(100, 100, 100, 100), "Test")) {
		//}
	}

	void Update ()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			if (Input.GetKey (KeyCode.Escape))
			{
				playerScript.WalkButtonToggled = false;
				possibleExit = true;
			}
		}

		if (refreshPath)
		{
			refreshPath = false;
			Debug.Log ("refreshing path");

			foreach (GameObjectPosition PathArrow in PathArrows)
			{
				Destroy (PathArrow.gameObject);
			}
			PathArrows.Clear ();

			Position Destination = null;
			DoorPosition DestinationPosition = null;
			foreach (DoorPosition DoorPosition in levelInfo.DoorPositions)
			{
				if (!DoorPosition.door.GetComponent<doorScript> ().DoorTriggered)
				{
					DestinationPosition = DoorPosition;
					break;
				}
			}
			if (DestinationPosition == null)
			{
				Destination = levelInfo.FinalPosition;
			}
			else if (key1.GetComponent<KeyScript> ().KeyEnabled)
			{
				Destination = DestinationPosition.closerToPlayer;
			}
			else
			{
				Destination = DestinationPosition.keyPosition;
			}

			playerPosition = new Position (Mathf.RoundToInt (player.transform.position.x), Mathf.RoundToInt ((float)player.transform.position.z - 0.5f));
			PathFinder pathFinder = new PathFinder (levelInfo.GridX, levelInfo.GridY, levelInfo.WallsLeft);
			thePath = pathFinder.findPath (playerPosition, Destination, false);

			Position target = levelInfo.FinalPosition;
			for (int PositionIndex = 1; PositionIndex < thePath.Count-1; ++PositionIndex)
			{
				GameObject GameObject2 = (GameObject)Instantiate (arrowToExit, new Vector3 (thePath [PositionIndex].x, 0.0f, thePath [PositionIndex].y + 0.5f), Quaternion.identity);
				GameObject2.transform.Rotate (0, 90, 0);
				if (thePath [PositionIndex].x == target.x)
				{
					if (thePath[PositionIndex].y < target.y)
					{ }
					else
						GameObject2.transform.Rotate(0, 180, 0);
				}
				else if (thePath [PositionIndex].x > target.x)
					GameObject2.transform.Rotate (0, 270, 0);
				else
					GameObject2.transform.Rotate (0, 90, 0);


				PathArrows.Add (new GameObjectPosition (thePath [PositionIndex], GameObject2));
				target = thePath [PositionIndex];
			}

			--pathOwned;
			VarEnums.SetInt (VarEnums.PathOwned, pathOwned);
		}
	}

	private GameObject InstantiateWall (GameObject wall, Wall theWall)
	{
		return InstantiateWall (wall, theWall.X + (theWall.Rotated ? -0.5f : 0.0f), theWall.Y + (theWall.Rotated ? 0.5f : 0.0f), theWall.Rotated);
	}

	private GameObject InstantiateWall (GameObject wall, float x, float y, bool rotate)
	{
		GameObject GameObject = (GameObject)Instantiate (wall, new Vector3 (x, 0, y), Quaternion.identity);
		if (rotate)
			GameObject.transform.Rotate (new Vector3 (0, 90, 0));
		GameObject.transform.localScale += new Vector3 (-0.7f, -0.5f, -0.7f);
		return GameObject;
	}

	private void MenuOK (string ID)
	{
		//Debug.Log ("MenuOK: " + ID);
		if (ID == "pathBuy")
		{
			//GetComponent<InAppPurchases>().purchaseTestProduct();
			GetComponent<InAppPurchases> ().purchaseProduct ();
		}
		else if (ID == "pathUse")
		{
			refreshPath = true;
		}
		else if (ID == "exitPrompt")
		{
			VarEnums.SetInt (VarEnums.GameInProgress, 0);
			//Application.LoadLevel ("PickLevel");
			SceneManager.LoadScene("PickLevel");
		}
		DisableMenu ();
	}

	private void MenuCancel (string ID)
	{
		//Debug.Log ("MenuCancel: " + ID);
		DisableMenu ();
	}

	private void DisableMenu ()
	{
		displayPathBuyConfirmMenu = false;
		displayPathUseConfirmMenu = false;
		possibleExit = false;
	}

	private void SaveEverything ()
	{
		GameState GameState = new GameState ();
		GameState.playerPosition = new Position ((int)(this.player.transform.position.x * 1000), (int)(this.player.transform.position.z * 1000));
		GameState.playerRotation = (int)this.player.transform.rotation.eulerAngles.y;
		GameState.timeElapsed = (int)Utility_LevelTimer.GetTimePassed ().TotalSeconds;
		GameState.wallsLeft = levelInfo.WallsLeft;
		GameState.finalPosition = levelInfo.FinalPosition;
		GameState.haveKey = key1.activeSelf;
		GameState.doorPositions = DoorPosition.ToDoorPositionSerializable (levelInfo.DoorPositions);
		GameState.haveKey = key1.GetComponent<KeyScript> ().KeyEnabled;
		GameState.pathArrows = PathArrows;
		for (int i = GameState.pathArrows.Count - 1; i >= 0; --i)
		{
			if (!GameState.pathArrows [i].gameObject.activeSelf)
			{
				Destroy (GameState.pathArrows [i].gameObject);
				GameState.pathArrows.RemoveAt (i);
			}
		}

		GameState.current = GameState;
		SaveLoad.Save ();
	}

	void OnApplicationQuit ()
	{
		SaveEverything ();
	}

	void OnApplicationPause ()
	{
		//Utility_LevelTimer.PauseLevel ();
		SaveEverything ();
	}

	void OnApplicationFocus (bool focusStatus)
	{
		//Debug.Log ("OnApplicationFocus: " + focusStatus);
	}
}

public class DoorPosition
{
	internal GameObject door;
	internal GameObject key;
	public Position closerToPlayer;
	public Position closerToFinalPosition;
	public Position keyPosition;
	public bool doorTriggered;
	public bool keyPickedUp;

	public DoorPosition ()
	{

	}

	public DoorPosition (DoorPositionSerializable DoorPositionSerializable)
	{
		closerToPlayer = DoorPositionSerializable.closerToPlayer;
		closerToFinalPosition = DoorPositionSerializable.closerToFinalPosition;
		keyPosition = DoorPositionSerializable.keyPosition;
		doorTriggered = DoorPositionSerializable.doorTriggered;
		keyPickedUp = DoorPositionSerializable.keyPickedUp;
	}

	public DoorPosition (Position CloserToPlayer, Position CloserToFinalPosition)
	{
		door = null;
		key = null;
		closerToPlayer = CloserToPlayer;
		closerToFinalPosition = CloserToFinalPosition;
		doorTriggered = false;
		keyPickedUp = false;
		keyPosition = null;
	}

	internal GameObject Door { set { door = value; } }

	internal GameObject Key { set { key = value; } }

	public override string ToString ()
	{
		return string.Format ("[DoorPosition]" + closerToPlayer + closerToFinalPosition);
	}

	public void DoorTriggered ()
	{
		doorTriggered = true;
	}

	public void PickUpKey ()
	{
		keyPickedUp = true;
	}

	public static List<DoorPositionSerializable> ToDoorPositionSerializable (List<DoorPosition> Original)
	{
		List<DoorPositionSerializable> DoorPositionSerializable = new List<DoorPositionSerializable> ();
		foreach (DoorPosition LocalDoorPosition in Original)
		{
			if (LocalDoorPosition.door != null)
			{
				LocalDoorPosition.doorTriggered = LocalDoorPosition.door.GetComponent<doorScript> ().DoorTriggered;
			}
			if (LocalDoorPosition.key != null)
			{
				LocalDoorPosition.keyPickedUp = !LocalDoorPosition.key.activeSelf;
			}
			DoorPositionSerializable.Add (new DoorPositionSerializable (LocalDoorPosition));
		}
		return DoorPositionSerializable;
	}

	public static void NormalizeDoorsFromLevel (List<DoorPosition> Original)
	{
		foreach (DoorPosition LocalDoorPosition in Original)
		{
			if (LocalDoorPosition.door != null && LocalDoorPosition.doorTriggered)
			{
				LocalDoorPosition.door.GetComponent<doorScript> ().DoorTriggered = LocalDoorPosition.doorTriggered;
			}
			if (LocalDoorPosition.key != null && LocalDoorPosition.keyPickedUp)
			{
				LocalDoorPosition.key.SetActive (false);
			}
		}
	}
}

[System.Serializable]
public class DoorPositionSerializable
{
	public Position closerToPlayer;
	public Position closerToFinalPosition;
	public Position keyPosition;
	public bool doorTriggered;
	public bool keyPickedUp;

	public DoorPositionSerializable ()
	{
	}

	public DoorPositionSerializable (DoorPosition DoorPosition)
	{
		closerToPlayer = DoorPosition.closerToPlayer;
		closerToFinalPosition = DoorPosition.closerToFinalPosition;
		keyPosition = DoorPosition.keyPosition;
		doorTriggered = DoorPosition.doorTriggered;
		keyPickedUp = DoorPosition.keyPickedUp;
	}

	public static List<DoorPosition> ToDoorPosition (List<DoorPositionSerializable> Original)
	{
		List<DoorPosition> DoorPosition = new List<DoorPosition> ();
		foreach (DoorPositionSerializable DoorPositionSerializable in Original)
		{
			DoorPosition.Add (new DoorPosition (DoorPositionSerializable));
		}
		return DoorPosition;
	}
}

[System.Serializable]
public class GameObjectPosition
{
	public Position position = null;
	[System.NonSerialized()]
	public GameObject
		gameObject = null;

	public GameObjectPosition ()
	{
	}

	internal GameObjectPosition (Position Position, GameObject GameObject)
	{
		position = Position;
		gameObject = GameObject;
	}
}

public enum ButtonPosition
{
	left,
	right,
	centered,
}

internal class MyTimeSpan
{
	internal string name;
	internal System.TimeSpan timeSpan;

	internal MyTimeSpan (string Name, System.TimeSpan TimeSpan)
	{
		name = Name;
		timeSpan = TimeSpan;
	}

	public override string ToString ()
	{
		return string.Format (name + ":" + timeSpan.TotalMilliseconds);
	}
}


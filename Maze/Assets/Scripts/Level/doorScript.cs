﻿using UnityEngine;
using System.Collections;

public class doorScript : MonoBehaviour {
	public float targetPosition;

	private bool needsKey;
	public bool NeedsKey { get { return needsKey; } set { needsKey = value; } }

	private bool doorTriggered = false;

	public bool DoorTriggered {
		get { return doorTriggered; }
		set {
			if (!doorTriggered) {
				doorTriggered = true;

				if (Utility_Music.SoundOn) {
					GetComponent<AudioSource>().Play ();
				}

				MoveLerpObject MoveLerpObject = GetComponent<MoveLerpObject>();
				MoveLerpObject.finalPosition = new Vector3 (transform.position.x, targetPosition, transform.position.z);
				MoveLerpObject.enabled = true;
			}
		}
	}
}

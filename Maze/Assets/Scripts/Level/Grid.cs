﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid {

	int x;
	public int X { get { return x; } }
	int y;
	public int Y { get { return y; } }
	
	public Grid(int X, int Y) {
		x = X;
		y = Y;
	
		// first create walls
		for (int y_i = 0; y_i < y; ++y_i) {
			for (int x_i = 0; x_i < x; ++x_i) {
				positionsAll.Add (new Position(x_i, y_i));
				if (x_i > 0) {
					walls.Add(new Wall(x_i-1, y_i, x_i, y_i));
				}
				//if (x_i < x - 1) {
				//	walls.Add(new Wall(x_i+1, y_i, x_i, y_i));
				//}
				
				if (y_i > 0) {
					walls.Add(new Wall(x_i, y_i-1, x_i, y_i));
				}
				//if (y_i < y - 1) {
				//	walls.Add(new Wall(x_i, y_i+1, x_i, y_i+1));
				//}
			}
		}

		wallsLeft.AddRange (walls);
		
		positionsLeft.AddRange (positionsAll);
		positionsLeft.Shuffle ();

		MakeMaze ();
	}

	public override string ToString ()
	{
		return string.Format ("[Grid] " + x + "/" + y);
	}

	List<Wall> walls = new List<Wall>();
	List<Wall> wallsBroken = new List<Wall> ();
	List<Wall> wallsLeft = new List<Wall> ();
	public List<Wall> WallsLeft { get { return wallsLeft; } }
	List<Position> positionsAll = new List<Position> ();
	List<Position> positionsLeft = new List<Position> ();
	List<Position> positionsVisited = new List<Position> ();

//	private void MakeMaze_bad1()
//	{
//		Debug.Log ("At start of MakeMaze, there are " + wallsLeft.Count + " wallsLeft and " + wallsBroken.Count + " wallsBroken " + positionsLeft.Count + " positionsLeft ");
//		while (positionsLeft.Count > 0) {
//			Position position = positionsLeft[0];
//			List<Position> notVisited = GetNotVisitedAdjacent(position);
//			if (notVisited.Count > 0) {
//				Position adjacent = notVisited[Random.Range(0, notVisited.Count)];
//				Wall NewWall = new Wall(position, adjacent);
//				this.wallsBroken.Add (NewWall);
//				if (this.wallsLeft.Remove(NewWall))
//					Debug.Log ("Removed wall " + NewWall);
//				else
//					Debug.Log ("Failed to remove wall " + NewWall);
//			}
//			positionsLeft.Remove(position);
//		}
//		Debug.Log ("At end of MakeMaze, there are " + wallsLeft.Count + " wallsLeft and " + wallsBroken.Count + " wallsBroken " + positionsLeft.Count + " positionsLeft ");
//	}

	private void MakeMaze()
	{
		//Debug.Log ("At start of MakeMaze, there are " + wallsLeft.Count + " wallsLeft and " + wallsBroken.Count + " wallsBroken " + positionsLeft.Count + " positionsLeft ");
		Position CurrentPosition = positionsLeft [0];
		positionsLeft.Remove(CurrentPosition);
		positionsVisited.Add(CurrentPosition);
		//Debug.Log ("Starting at " + CurrentPosition);
		int maxI = 500;
		while (positionsLeft.Count > 0) {
			List<Position> notVisited = GetNotVisitedAdjacent(CurrentPosition);
			if (notVisited.Count > 0) {
				Position adjacent = notVisited[Random.Range(0, notVisited.Count)];
				Wall NewWall = new Wall(CurrentPosition, adjacent);
				this.wallsBroken.Add (NewWall);
				this.wallsLeft.Remove(NewWall);
				positionsLeft.Remove(adjacent);
				/*
				if (this.wallsLeft.Remove(NewWall))
					Debug.Log ("Removed wall " + NewWall);					
				else
					Debug.Log ("Failed to remove wall " + NewWall);					

				if (positionsLeft.Remove(adjacent))
					Debug.Log("Removed adjacent " + adjacent);
				else
					Debug.Log ("failed to remove position " + adjacent);	
				*/

				positionsVisited.Add(CurrentPosition);
				CurrentPosition = adjacent;
				//Debug.Log ("New position: " + CurrentPosition);
			}
			else if (positionsVisited.Count > 0) {
				//Debug.Log("notVisisted.count is 0");
				CurrentPosition = positionsVisited[positionsVisited.Count-1];
				positionsVisited.Remove(CurrentPosition);
				//positionsVisited.Insert(0, CurrentPosition);
				//Debug.Log ("New position: " + CurrentPosition);
			}
			else {
				//Debug.Log("where are we?");
				CurrentPosition = positionsLeft [0];
				positionsLeft.Remove(CurrentPosition);
				//positionsVisited.Add(CurrentPosition);
			}
			--maxI;
			//if (maxI <= 0)
			//	break;
		}

		//fixRestMaze ();
		//Debug.Log ("maxI is " + maxI + " At end of MakeMaze, there are " + wallsLeft.Count + " wallsLeft and " + wallsBroken.Count + " wallsBroken " + positionsLeft.Count + " positionsLeft ");
	}

	// i think this can be removed
//	private void fixRestMaze() {
//		bool AllPathsAccessZero;
//		do {
//			AllPathsAccessZero = false;
//
//			Position zero = new Position(0, 0);
//			for (int x = 0; x < X; ++x) {
//				for (int y = 0; y < Y; ++y) {
//					if (x > 0 || y > 0) {
//						PathFinder pathFinder = new PathFinder(X, Y, WallsLeft);
//						Position currentPosition = new Position(x, y);
//						if (pathFinder.findPath(zero, currentPosition, false) == null) {
//							AllPathsAccessZero = true;
//
//							Wall tryWall;
//							List<Wall> positionWalls = new List<Wall>();
//							tryWall = new Wall(currentPosition, new Position(x-1, y));
//							if (wallsLeft.Contains(tryWall))
//								positionWalls.Add(tryWall);
//							tryWall = new Wall(currentPosition, new Position(x+1, y));
//							if (wallsLeft.Contains(tryWall))
//								positionWalls.Add(tryWall);
//							tryWall = new Wall(currentPosition, new Position(x, y-1));
//							if (wallsLeft.Contains(tryWall))
//								positionWalls.Add(tryWall);
//							tryWall = new Wall(currentPosition, new Position(x, y+1));
//							if (wallsLeft.Contains(tryWall))
//								positionWalls.Add(tryWall);
//
//							if (positionWalls.Count > 0) {
//								int doorLocationInPath = Random.Range (0, positionWalls.Count-1);
//								this.wallsBroken.Add (positionWalls[doorLocationInPath]);
//								this.wallsLeft.Remove(positionWalls[doorLocationInPath]);
//							}
//						}
//					}
//				}
//			}
//		} while (AllPathsAccessZero);
//	}

	private List<Position> GetListIfValid(Position position)
	{
		List<Position> ret = new List<Position> ();
		if (positionsLeft.Contains (position)) {
			//Debug.Log ("contained " + position);
			ret.Add (position);
		} 
		//else
		//	Debug.Log ("did not contain " + position);
		return ret;
	}
	private List<Position> GetNotVisitedAdjacent(Position position)
	{
		List <Position> ret = new List<Position>();
		ret.AddRange (GetListIfValid (new Position (position.x - 1, position.y)));
		ret.AddRange (GetListIfValid (new Position (position.x + 1, position.y)));
		ret.AddRange (GetListIfValid (new Position (position.x, position.y-1)));
		ret.AddRange (GetListIfValid (new Position (position.x, position.y+1)));
		return ret;
	}
	
	public void PrintWalls() {
		PrintWalls (walls);
		PrintWalls (wallsBroken);
	}
	private void PrintWalls(List <Wall>PrintMe)
	{	
		
		int wall_i = 0;
		foreach (Wall wall in PrintMe) {
			Debug.Log("" + (++wall_i).ToString() + ") " + wall);
		}
	}

	//private string SerializeWalls(string Name, List<Wall> Walls) {
	//	string RetVal="[" + Name + "](";
	//	foreach (Wall Wall in Walls) {
	//		RetVal += Wall.Serialize() + ",";
	//	}
	//	return RetVal + ")";
	//}
	private string SerializePositions(string Name, List<Position> Positions) {
		string RetVal="[" + Name + "](";
		foreach (Position Position in Positions) {
			RetVal += Position.Serialize() + ",";
		}
		return RetVal + ")";
	}

//	public string Serialize() {
//		string RetVal = string.Format ("[Grid]({0},{1}){", this.x, this.y);
//		RetVal += SerializeWalls ("walls", walls) + ",";
//		RetVal += SerializeWalls ("wallsBroken", wallsBroken) + ",";
//		RetVal += SerializeWalls ("wallsLeft", wallsLeft) + ",";
//
//		RetVal += SerializePositions ("positionsAll", positionsAll) + ",";
//		RetVal += SerializePositions ("positionsLeft", positionsLeft) + ",";
//		RetVal += SerializePositions ("positionsVisited", positionsVisited) + ",";
//
//		return RetVal + ")";
//	}
}

[System.Serializable]
public class Wall {
	public Position one;
	public Position two;
	public Position One { get { return one; } }
	public Position Two { get { return two; } }
	public Wall () {
	}
	public Wall (int x1, int y1, int x2, int y2) : this(new Position(x1, y1), new Position(x2, y2)) {
	}
	public Wall (Position _one, Position _two) {
		one = new Position(_one.x, _one.y);
		two = new Position(_two.x, _two.y);
	}
	public override bool Equals (object obj)
	{
		if (obj is Wall) {
			if ((obj as Wall).one.Equals(one) && (obj as Wall).two.Equals(two)) {
				return true;
			}
			if ((obj as Wall).one.Equals(two) && (obj as Wall).two.Equals(one)) {
				return true;
			}
		}
		return false;
	}
	public override string ToString ()
	{
		return string.Format ("[Wall] " + one + " " + two);
	}
	public override int GetHashCode ()
	{
		return new Vector2(one.x + two.x, one.y + two.y).GetHashCode ();
	}
	public int X {
		get {
			return Mathf.Max(one.x, two.x);
		}
	}
	public int Y {
		get {
			return Mathf.Max(one.y, two.y);
		}
	}
	public bool Rotated {
		get {
			return one.x != two.x;
		}
	}

	//public string Serialize() {
	//	return string.Format ("[Wall]({0},{1})", one.Serialize (), two.Serialize ());
	//}

	public Wall DeepCopy() {
		return new Wall (one.DeepCopy (), two.DeepCopy ());
	}

	public static List<Wall> CopyList(List<Wall> Walls) {
		List<Wall> ReturnWalls = new List<Wall>();
		foreach (Wall Wall in Walls) {
			ReturnWalls.Add(Wall.DeepCopy());
		}
		return ReturnWalls;
	}
}

[System.Serializable]
public class Position
{
	public int x;
	public int y;
	public Position () {
	}

	public Position (int X, int Y) {
		x = X;
		y = Y;
	}
	public override string ToString ()
	{
		return x + "/" + y;
	}

	public override bool Equals (object obj)
	{
		if (obj is Position) {
			if ((obj as Position).x == x && (obj as Position).y == y) {
				return true;
			}
		}
		return false;
	}
	public override int GetHashCode ()
	{
		return new Vector2(x, y).GetHashCode ();
	}
	public string Serialize() {
		return string.Format ("[Position]({0},{1})", this.x, this.y);
	}

	public Position DeepCopy() {
		return new Position (x, y);
	}
}

static class MyExtensions
{
	public static void Shuffle<T>(this IList<T> list)
	{ 
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}
}

/////////////
/// 
/// 

	public class MazeGenerator
	{
		public static bool Randomize { get; set; }
		//private static Random rand = new Random(); // You probably want to seed this
		
		public static void Seed(int seed)
		{
			//rand = new Random(seed);
		}
		
		// The actual maze generation is adaptible to any topology
		private static void MakeMaze(GenerationCell start)
		{
			GenerationCell current = start, exit = start, next;
			ulong depth = 0, max = 0; // To find the farthest exit, we keep track of “tree” depth
			// Potential improvement: Store this information in the cell itself, for fancy stuff later on
			
			while (true) // This ain't Lisp, I ain't recursin'
			{
				current.Visited = true;
				
				if (Randomize)
					Shuffle<Cell>(current.Links); // This is a destructive function. May want to rewrite to use a temporary clone list for iteration
				
				bool natural = true;
				for (int i = 0; i < current.Links.Count; i++ )
				{
					next = (GenerationCell)current.Links[i]; // We cast for access to .Previous, which regular cells do not have // Note: This incurs no extra penalty so using it in the main body of a loop is A-OK
					
					if (next.Previous == current) // We obviously don't want to mess with a link if we've already gone that way
						continue;
					
					if (next.Visited) // It was already visited via some other route, so this is not what we're looking for
					{
						current.Links.RemoveAt(i--);
						continue;
					}
					
					// It's an unexplored link
					if (++depth > max) // Since we're exploring, and want to keep track of the highest later on
					{
						exit = next;
						max = depth;
					}
					
					next.Previous = current;
					current = next;
					natural = false; // Ugly helper variable since we can't continue out of multiple loops that easily
					break;
				}
				
				if (!natural) // The loop didn't terminate naturally, so we found a new node and are considering from there
					continue;
				
				// We explored all links and found none - dead end cell
				if (current.Previous == null) // The starting cell has no previous cell
				{
					// The function can safely end here, since if we've backtraced all the way to the
					// starting cell and found no more link, the entire maze has been explored.
					exit.ExitCell = true;
					return;
				}
				
				current = current.Previous;
				depth--;
			}   // The endless loop repeats here
		}
		
		
		
		// Generates an array of cells, and a maze to go with them
		public static Cell[,] GenerateOrthogonal(int Width, int Height, int XEntrance, int YEntrance, bool Cyclic)
		{
			if (XEntrance >= Width || YEntrance >= Height)
				throw new System.Exception("Starting coordinates out of range.");
				//throw new ArgumentOutOfRangeException("Starting coordinates out of range.");
			
			GenerationCell[,] maze = new GenerationCell[Width, Height];
			
			for (int x = 0; x < Width; x++)
				for (int y = 0; y < Height; y++)
					maze[x, y] = new GenerationCell(); // initialize with non-connected cells
			
			// Generate links to adjacent cells, since this is an orthogonal maze
			// To-do: Maybe encapsulate the bounds-checking and link generation process inside a small function, and call it on a list of locations instead?
			//        Not going to bother right now since it won't significantly reduce SLOC count, but for non-2D mazes this would be nice
			for (int x = 0; x < Width; x++)
				for (int y = 0; y < Height; y++)
			{
				if (x > 0)
					maze[x, y].Links.Add(maze[x - 1, y]);
				else if (Cyclic)
					maze[x, y].Links.Add(maze[Width - 1, y]);
				
				if (y > 0)
					maze[x, y].Links.Add(maze[x, y - 1]);
				else if (Cyclic)
					maze[x, y].Links.Add(maze[x, Height - 1]);
				
				if (x + 1 < Width)
					maze[x, y].Links.Add(maze[x + 1, y]);
				else if (Cyclic)
					maze[x, y].Links.Add(maze[0, y]);
				
				if (y + 1 < Height)
					maze[x, y].Links.Add(maze[x, y + 1]);
				else if (Cyclic)
					maze[x, y].Links.Add(maze[x, 0]);
			}
			
			MakeMaze(maze[XEntrance, YEntrance]);
			
			return maze;
		}
		
		
		private static void Shuffle<T>(List<T> list) // pseudo-polymorphism hack, cheaper than boxing/unboxing. Haskell has spoiled me.
		{
			for (int n = list.Count - 1; n > 0; n--) // Note: Fisher-Yates shuffle
			{
				int k = Random.Range(0, n+1);
				//int k = rand.Next(n + 1);
				T tmp = list[k];
				list[k] = list[n];
				list[n] = tmp;
			}
		}
		
		private class GenerationCell : Cell // Helper class to store information not relevant to the end result
		{
			public bool Visited = false;
			public GenerationCell Previous = null; // For stackless backtracking
			
			public GenerationCell() // Cell has no constructor
			{
				
			}
		}
	}
	
	public class Cell
	{
		public List<Cell> Links = new List<Cell>();
		public bool ExitCell = false;
		
		public bool IsLinked(Cell c) // Not actually used during generation logic, but this can be useful for drawing
		{
			return Links.Contains(c) || c.Links.Contains(this);
		}
		
		public bool IsEdge
		{
			get { return Links.Count == 0; }
		}
	}



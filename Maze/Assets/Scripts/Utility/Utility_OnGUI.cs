﻿using UnityEngine;
using System.Collections;

public class Utility_OnGUI : MonoBehaviour {
	private static GUIStyle guiStyle = null;

	private static void InitGUIStyle() {
		if (guiStyle == null) {
			guiStyle = new GUIStyle();
			guiStyle.fontSize = Screen.height / 14;
			guiStyle.normal.textColor = Color.blue;
			//guiStyle.fontStyle = FontStyle.BoldAndItalic;
			guiStyle.alignment = TextAnchor.MiddleCenter;
		}
	}

	private static bool _Button(bool RepeatedButton, Rect Position, string text, Texture Texture = null) {
		InitGUIStyle ();

		Color BackgroundColor = GUI.backgroundColor;
		GUI.backgroundColor = Color.clear;
		if (Texture != null) {
			//Debug.Log ("Position = " + Position + " Texture " + Texture);
			GUI.DrawTexture (Position, Texture);
		}
		bool RetVal;
		if (RepeatedButton) {
			RetVal = GUI.RepeatButton (Position, text, guiStyle);
		}
		else {
			RetVal = GUI.Button (Position, text, guiStyle);
		}
		GUI.backgroundColor = BackgroundColor;
		return RetVal;
	}

	public static bool Button(Rect Position, string text, Texture Texture = null) {
		return _Button (false, Position, text, Texture);
	}

	public static bool Button(Rect Position, Texture Texture) {
		return _Button (false, Position, string.Empty, Texture);
	}

	public static bool RepeatButton(Rect Position, string text, Texture Texture = null) {
		return _Button (true, Position, text, Texture);
	}
	
	public static bool RepeatButton(Rect Position, Texture Texture) {
		return _Button (true, Position, string.Empty, Texture);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Utility_LevelTimer : MonoBehaviour {
	private static System.DateTime startTime = System.DateTime.Now;
	private static System.TimeSpan passedTime;

	public static void StartLevel() {
		startTime = System.DateTime.Now;
		passedTime = new System.TimeSpan ();
	}

	public static void PauseLevel() {
		passedTime = GetTimePassed();
	}

	public static void UnPauseLevel() {
		startTime = System.DateTime.Now;
	}

	public static System.TimeSpan GetTimePassed() {
		return passedTime.Add(System.DateTime.Now.Subtract(startTime));
	}
	public static System.TimeSpan SetTimePassed(int Seconds) {
		return passedTime = new System.TimeSpan(0, 0, Seconds);
	}

	public static System.TimeSpan FinishLevel() {
		System.TimeSpan TimeElapsed = GetTimePassed ();
		int Level = VarEnums.GetInt (VarEnums.NextLevel, 0);
		Debug.Log ("for level " + Level + " saving " + ((int)TimeElapsed.TotalSeconds) + " seconds");
		int NewBestTime = Mathf.Min(VarEnums.GetInt (VarEnums.LevelTimePrefix + Level, 999), (int)TimeElapsed.TotalSeconds);
		VarEnums.SetInt (VarEnums.LevelTimePrefix + Level, NewBestTime);
		return TimeElapsed;
	}
}

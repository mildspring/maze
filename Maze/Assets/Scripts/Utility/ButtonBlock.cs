﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void ButtonCallback (string Id);

public class ButtonBlock
{
	public enum Orientation
	{
		Left,
		Right,
		Center,
	}

	private EventHandler eventHandler;
	private int buttonSize = 5;
	private Orientation justified = Orientation.Right;
	private List<ButtonEntity> buttonEntities = new List<ButtonEntity> ();
	private Rect rect = new Rect ();

	public Orientation Justified
	{
		get { return justified; }
		set { justified = value; }
	}

	public void AddButton (Texture Texture, EventHandlerValues EventHandlerValue)
	{
		buttonEntities.Add (new ButtonEntity (Texture, EventHandlerValue));
	}

	public ButtonBlock (EventHandler EventHandler)
	{
		eventHandler = EventHandler;
	}

	public void CallOnGUI ()
	{
		float OffsetUI = Mathf.Min (Screen.height, Screen.width) / 30;
		float totalLength = buttonEntities.Count * (OffsetUI * 6) - OffsetUI;
		float leftOffset;
		switch (Justified)
		{
			case Orientation.Left:
				leftOffset = OffsetUI;
				break;
			case Orientation.Center:
				leftOffset = (Screen.width - totalLength) / 2;
				break;
			case Orientation.Right:
			default:
				leftOffset = Screen.width - totalLength - OffsetUI;
				break;
		//default:
		// should never be here
		//	break;
		}

		rect.x = leftOffset;
		rect.y = Screen.height - OffsetUI * (buttonSize + 1);
		rect.width = OffsetUI * buttonSize;
		rect.height = OffsetUI * buttonSize;
		foreach (ButtonEntity ButtonEntity in buttonEntities)
		{
			if (Utility_OnGUI.Button (rect, ButtonEntity.texture))
			{
				eventHandler.TriggerEvent (ButtonEntity.eventHandlerValue, 1);
			}
			rect.x += OffsetUI * (buttonSize + 1);
		}
	}
}

internal class ButtonEntity
{
	internal Texture texture;
	internal EventHandlerValues eventHandlerValue;

	internal ButtonEntity (Texture Texture, EventHandlerValues EventHandlerValue)
	{
		texture = Texture;
		eventHandlerValue = EventHandlerValue;
	}
}
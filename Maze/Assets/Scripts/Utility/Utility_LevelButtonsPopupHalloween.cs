﻿using UnityEngine;
using System.Collections;

public class Utility_LevelButtonsPopupHalloween : MonoBehaviour {
	public GUISkin MySkin;

	public Texture star;
	public Texture twoStars;
	public Texture threeStars;
	
	private Rect windowRect0; // = new Rect (500, 140, 350, 510);
	private int spikeCount;
	private bool toggleBtn;
	
	private string id;
	private string text;
	private Utility_Menu_OKButton okCallback;
	private Utility_Menu_CancelButton cancelCallback;
	
	private float leafOffset;
	private float frameOffset;
	private float skullOffset;
	
	float originalWidth = 580;
	float originalHeight = 440;

	private int threeStarsSeconds;
	private int twoStarsSeconds;
	private int oneStarSeconds;

	void Start() {
		int MenuSide = Screen.height;
		windowRect0 = new Rect (Screen.width / 2 - MenuSide / 2, Screen.height - MenuSide, MenuSide, MenuSide);
		
		Vector2 ratio = new Vector2(Screen.width/originalWidth , Screen.height/originalHeight );
		windowRect0.x /= ratio.x;
		windowRect0.width /= ratio.x;
		windowRect0.y /= ratio.y;
		windowRect0.height /= ratio.y;
	}
	
	private void FancyTop(float TopX)
	{
		leafOffset = (TopX/2)-64;
		frameOffset = (TopX/2)-27;
		skullOffset = (TopX/2)-20;
		GUI.Label(new Rect(leafOffset, 18, 0, 0), "", "GoldLeaf");//-------------------------------- custom	
		GUI.Label(new Rect(frameOffset, 3, 0, 0), "", "IconFrame");//-------------------------------- custom	
		GUI.Label(new Rect(skullOffset, 12, 0, 0), "", "Skull");//-------------------------------- custom	
	}
	
	private void AddSpikes(float WinX) {
		spikeCount = (int) Mathf.Floor(WinX - 152)/22;
		GUILayout.BeginHorizontal();
		GUILayout.Label ("", "SpikeLeft");//-------------------------------- custom
		for (int i = 0; i < spikeCount; i++)
		{
			GUILayout.Label ("", "SpikeMid");//-------------------------------- custom
		}
		GUILayout.Label ("", "SpikeRight");//-------------------------------- custom
		GUILayout.EndHorizontal();
	}
	
	private void DoMyWindow1 (int WindowID) {
        //		float ButtonHeight = Screen.height / 6;
        //		float YOffset = ButtonHeight / 2;
        //		float ButtonWidth = ButtonHeight * 5;
        //

        var myStyle = GUI.skin.GetStyle("Label");
        myStyle.wordWrap = false;

        //GUIStyle nonbreakingLabelStyle;
        //nonbreakingLabelStyle = new GUIStyle();
        //nonbreakingLabelStyle.wordWrap = false;
        //nonbreakingLabelStyle.normal.textColor = Color.white;

        // use the spike function to add the spikes
        // note: were passing the width of the window to the function
        AddSpikes(windowRect0.width);

        //FancyTop(windowRect0.width);

        GUILayout.BeginVertical();
		//GUILayout.Space(8);
		//GUILayout.Label("", "Divider");//-------------------------------- custom
		
		GUILayout.Box(text);
		
		//GUILayout.Label("", "Divider");//-------------------------------- custom

		GUILayout.BeginHorizontal();
		//GUILayout.Label(star, new GUILayoutOption[] { GUIStyle.
		GUILayout.Label(threeStars);//-------------------------------- custom	
		//GUILayout.Label("x 3");
		//GUILayout.Label(star, GUILayout.Width(star.width*2));//-------------------------------- custom	
		//GUILayout.Label(star, GUILayout.Width(star.width*2));//-------------------------------- custom	
		GUILayout.FlexibleSpace();
		GUILayout.Label(Utility_LevelButtonsPopup.GetStarTime(threeStarsSeconds, true), myStyle);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label(twoStars);//-------------------------------- custom	
		//GUILayout.Label("x 2");
		//GUILayout.Label(star, GUILayout.Width(star.width*2));//-------------------------------- custom	
		GUILayout.FlexibleSpace();
		GUILayout.Label(Utility_LevelButtonsPopup.GetStarTime(twoStarsSeconds, true), myStyle);
		//GUILayout.Label("3 min 20 sec");

		//GUILayout.Label("Skull");//-------------------------------- custom	
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label(star);//-------------------------------- custom	
		GUILayout.FlexibleSpace();
		GUILayout.Label(Utility_LevelButtonsPopup.GetStarTime(oneStarSeconds, true), myStyle);
		//GUILayout.Label("Skull");//-------------------------------- custom	
		//GUILayout.Label("Skull");//-------------------------------- custom	
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		if (okCallback != null) {
			if (GUILayout.Button ("Yes",  GUILayout.Height(windowRect0.width/3))) {
			//if (GUILayout.Button ("Yes")) {
				okCallback(id);
			}
		}
		
		if (cancelCallback != null) {
			if (GUILayout.Button ("No",  GUILayout.Height(windowRect0.width/3))) {
			//if (GUILayout.Button ("No")) {
				cancelCallback(id);
			}
		}
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
		
	}

	// Update is called once per frame
	public void Popup(int Level, int ThreeStarsSeconds, int TwoStarsSeconds, int OneStarSeconds, int BestTime, string ID, Utility_Menu_OKButton OKCallback, Utility_Menu_CancelButton CancelCallback) {
		//Debug.Log ("Popup Start");
		threeStarsSeconds = ThreeStarsSeconds;
		twoStarsSeconds = TwoStarsSeconds;
		oneStarSeconds = OneStarSeconds;
		id = ID;
		text = "Level " + Level;
		okCallback = OKCallback;
		cancelCallback = CancelCallback;
		
		GUI.skin = MySkin;
		
		// from http://answers.unity3d.com/questions/359320/relate-font-size-to-screen-size-in-guiskin.html
		Vector2 ratio = new Vector2(Screen.width/originalWidth , Screen.height/originalHeight );
		Matrix4x4 guiMatrix = Matrix4x4.identity;
        //guiMatrix.SetTRS(new Vector3(1, 1, 1), Quaternion.identity, new Vector3(ratio.x, ratio.y, 1));
        guiMatrix.SetTRS(new Vector3(1, 1, 1), Quaternion.identity, new Vector3(ratio.x, ratio.y, 1));
        GUI.matrix = guiMatrix;
		
		windowRect0 = GUI.Window (0, windowRect0, DoMyWindow1, "");
		//now adjust to the group. (0,0) is the topleft corner of the group.
		GUI.BeginGroup (new Rect (0,0,100,100));
		// End the group we started above. This is very important to remember!
		GUI.EndGroup ();
		
		GUI.matrix = Matrix4x4.identity;
	}
}

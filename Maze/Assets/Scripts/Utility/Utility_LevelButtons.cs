﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Utility_LevelButtons : MonoBehaviour {
	public Texture button;
	public Texture buttonSelected;
	public Texture lockTexture;
	public Texture starTexture;

	private GUIStyle guiStyle;
	private Rect rect;

	private List<int> levelBestTimes = new List<int> ();

	private LevelSetup levelSetup;

	static private Utility_LevelButtons instance;

	void Awake() {
		instance = this;
		levelSetup = GetComponent<LevelSetup> ();
		Debug.Log ("Assigned instance to this = " + this);
	}

	// Use this for initialization
	void Start () {
		Debug.Log ("Unity_LevelButtons Start()");
		rect = new Rect ();
		guiStyle = new GUIStyle ();
		guiStyle.fontSize = Screen.height / 14;
		guiStyle.normal.textColor = Color.black;
		guiStyle.alignment = TextAnchor.MiddleCenter;

		SetLevelBestTimes ();

		//VarEnums.SetInt (VarEnums.TotalStars, TotalStars);
	}

	private void SetLevelBestTimes() {
		levelBestTimes.Clear ();
		for (int i = 0;; ++i) {
			int BestTime = VarEnums.GetInt(VarEnums.LevelTimePrefix + (i+1), -1);
			if (BestTime == -1) {
				break;
			}
			
			//Debug.Log ("BestTime level " + (i+1) + " is " + BestTime);
			levelBestTimes.Add(BestTime);
		}
	}

	public static int GetTotalStars() {
		int TotalStars = 0;

		instance.SetLevelBestTimes ();
		for (int i = 0; i < instance.levelBestTimes.Count; ++i)
		{
			LevelProperties LevelProperties = instance.levelSetup.Level(i+1);
			if (instance.levelBestTimes[i] <= LevelProperties.SecondsThreeStars)
				TotalStars += 3;
			else if (instance.levelBestTimes[i] <= LevelProperties.SecondsTwoStars)
				TotalStars += 2;
			else if (instance.levelBestTimes[i] <= LevelProperties.SecondsOneStar)
				++TotalStars;
		}

		return TotalStars;
	}

	/// <summary>
	/// Buttons the specified x and y.
	/// </summary>
	/// <param name="x">Make sure X is bigger than Y</param>
	/// <param name="y"></param>
	public int Buttons(int X, int Y, int StartingLevel, int MaxLevel, int SelectedLevel, float LeftOffset, float RightOffset, float TopOffset) {
		float ButtonSize = Screen.height / (X + 1);
		float Offset = ButtonSize / (X + 2);
		float LockSize = ButtonSize / 2;
		for (int y = 0; y < Y; ++y) {
			for (int x = 0; x < X; ++x) {
				int LevelNumber = StartingLevel + y * X + x;

				rect.x = Screen.width - LeftOffset - ((X - x) * (ButtonSize + Offset));
				rect.y = (y) * (ButtonSize + Offset) + Offset /* + TopOffset */ + ButtonSize;
				rect.width = ButtonSize;
				rect.height = ButtonSize;

				bool gotOne;

				if (LevelNumber > MaxLevel) {
					GUI.DrawTexture(rect, button);

					rect.x += ButtonSize - LockSize;
					rect.y += ButtonSize - LockSize;
					rect.width = LockSize;
					rect.height = LockSize;
					GUI.DrawTexture(rect, lockTexture);

					gotOne = false;
				}
				else {
					//Debug.Log (" " + LevelNumber + " " + SelectedLevel + " " + (LevelNumber == SelectedLevel)  );
					gotOne = Utility_OnGUI.Button(rect, (LevelNumber == SelectedLevel) ? buttonSelected : button);

					if (this.levelBestTimes.Count >= LevelNumber) {
						int BestTime = this.levelBestTimes[LevelNumber - 1];
						if (BestTime <= this.levelSetup.Level(LevelNumber).SecondsOneStar) {
							float StarSize = LockSize / 2;
							rect.x += ButtonSize - StarSize * 3;
							rect.y += ButtonSize - StarSize;
							rect.width = StarSize;
							rect.height = StarSize;
							GUI.DrawTexture(rect, starTexture);

							if (BestTime <= this.levelSetup.Level(LevelNumber).SecondsTwoStars) {
								rect.x += StarSize;
								GUI.DrawTexture(rect, starTexture);

								if (BestTime <= this.levelSetup.Level(LevelNumber).SecondsThreeStars) {
									rect.x += StarSize;
									GUI.DrawTexture(rect, starTexture);
								}
							}
						}
					}
				}

				rect.x = Screen.width - LeftOffset - ((X - x) * (ButtonSize + Offset));
				rect.y = (y) * (ButtonSize + Offset) + Offset - ButtonSize / 9 /* + TopOffset */ + ButtonSize;
				rect.width = ButtonSize;
				rect.height = ButtonSize;
				GUI.Label(rect, LevelNumber.ToString(), guiStyle);

				if (gotOne)
				{
					return LevelNumber;
				}
			}
		}

		return 0;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;

public static class SaveLoad {
	public static List<GameState> savedGames = new List<GameState>();

	public static void Save() {
		savedGames.Clear ();
		savedGames.Add(GameState.current);
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/savedGames.maze");
		bf.Serialize(file, SaveLoad.savedGames);
		file.Close();
	}

	public static bool Load() {
		if(File.Exists(Application.persistentDataPath + "/savedGames.maze")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedGames.maze", FileMode.Open);
			SaveLoad.savedGames = (List<GameState>)bf.Deserialize(file);
			file.Close();
			if (savedGames.Count > 0) {
				GameState.current = savedGames[0];
				return true;
			}	
		}
		return false;
	}
}

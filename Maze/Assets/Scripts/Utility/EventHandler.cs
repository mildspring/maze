﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void EventCallback (EventHandlerValues ID,int Value);

public class EventHandler : MonoBehaviour
{
	private Dictionary <EventHandlerValues, List<EventCallback>> eventCallbacks = new Dictionary<EventHandlerValues, List<EventCallback>> ();

	public void AddEvent (EventHandlerValues ID, EventCallback EventCallback)
	{
		List<EventCallback> ListEventCallback;
		if (!eventCallbacks.TryGetValue (ID, out ListEventCallback))
		{
			ListEventCallback = new List<EventCallback> ();
			eventCallbacks.Add (ID, ListEventCallback);
		}

		ListEventCallback.Add (EventCallback);
	}

	public void TriggerEvent (EventHandlerValues ID, int Value)
	{
		List<EventCallback> ListEventCallback;
		if (eventCallbacks.TryGetValue (ID, out ListEventCallback))
		{
			foreach (EventCallback EventCallback in ListEventCallback)
			{
				EventCallback (ID, Value);
			}
		}
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

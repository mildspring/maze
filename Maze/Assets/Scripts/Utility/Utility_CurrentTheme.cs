﻿using UnityEngine;
using System.Collections;

public class Utility_CurrentTheme : MonoBehaviour {
	private static Themes currentTheme;

	private static bool? themeEnabled;

	// Use this for initialization
	private static void GetTheme() {
		currentTheme = ThemeEnabled ? ThemeAvailable : Themes.Standard;
	}

	/// <summary>
	/// Returns true if theme is enabled
	/// </summary>
	/// <value><c>true</c> if theme enabled; otherwise, <c>false</c>.</value>
	public static bool ThemeEnabled
	{
		get {
			if (themeEnabled == null) {
				themeEnabled = (VarEnums.GetInt (VarEnums.ThemeEnabled, 1) == 1);
			}
			return (bool) themeEnabled;
		}
	}

	public static void Toggle() {
		themeEnabled = !ThemeEnabled;
		GetTheme ();
		VarEnums.SetInt (VarEnums.ThemeEnabled, ThemeEnabled ? 1 : 0);
	}

	/// <summary>
	/// Check which theme is available, even if it's not enabled
	/// </summary>
	/// <value>The theme available.</value>
	public static Themes ThemeAvailable {
		get {
			System.DateTime Now = System.DateTime.Now;
			//Debug.Log ("Month = " + Now.Month + " day = " + Now.day);
			if ((Now.Month == 10 && Now.Day >= 15) ||
			    (Now.Month == 11 && Now.Day <= 15)) {
				return Themes.Halloween;
			}

			return Themes.Standard;
		}
	}
	
	public static Themes CurrentTheme {
		get {
			if (currentTheme == Themes.None) {
				GetTheme ();
			}
			return currentTheme;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public delegate void Utility_Menu_OKButton (string ID);

public delegate void Utility_Menu_CancelButton (string ID);

public class Utility_Menu : MonoBehaviour
{
	public Texture background;
	public Texture OK;
	public Texture Cancel;
	private GUIStyle textGUIStyle;
	private Utility_MenuHalloween utility_MenuHalloween;
	private string popupID = "InternalPopupID";
	private string popupText = "";
	private string messageOnScreen = null;

	// Use this for initialization
	void Start ()
	{
		textGUIStyle = new GUIStyle ();
		textGUIStyle.fontSize = Screen.height / 14;
		textGUIStyle.normal.textColor = Color.blue;
		textGUIStyle.alignment = TextAnchor.MiddleCenter;

		utility_MenuHalloween = GetComponent<Utility_MenuHalloween> ();
	}

	private void PopupCallback (string ID)
	{
		if (ID == popupID)
		{
			popupText = "";
		}
	}

	public void Popup (string Text)
	{
		if (string.IsNullOrEmpty (popupText))
		{
			popupText = Text;
		}
		else
		{
			popupText += "\n" + Text;
		}
	}

	public string MessageOnScreen
	{
		set
		{
			if (string.IsNullOrEmpty(value))
			{
				messageOnScreen = null;
			}
			else
			{
				
				messageOnScreen = value;
			}
		}
	}

	private void OnGUI ()
	{
		if (messageOnScreen != null)
		{
			Menu ("yyy", messageOnScreen, null, null);
		}
		if (!string.IsNullOrEmpty (popupText))
		{
			Menu (this.popupID, popupText, PopupCallback, null);
		}
	}

	/// <summary>
	/// Should be called from OnGUI to display menu.
	/// </summary>
	/// <param name="ID">I.</param>
	/// <param name="Text">Text.</param>
	/// <param name="OKCallback">OK callback.</param>
	/// <param name="CancelCallback">Cancel callback.</param>
	public void Menu (string ID, string Text, Utility_Menu_OKButton OKCallback, Utility_Menu_CancelButton CancelCallback)
	{
		utility_MenuHalloween.Menu (ID, Text, OKCallback, CancelCallback);
	}
	
}

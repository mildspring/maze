﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ThemeButton : MonoBehaviour {
	public Texture themeHalloweenOn;
	public Texture themeHalloweenOff;

	public Texture buttonOn;
	public Texture buttonOff;

	public List<GameObject> themeGameObjects;

	void Start() {
		//Debug.Log ("OnStart ThemeButton, theme enabled: " + Utility_CurrentTheme.ThemeEnabled);
		foreach (GameObject themeGameObject in themeGameObjects) {
			themeGameObject.SetActive(Utility_CurrentTheme.ThemeEnabled && Utility_CurrentTheme.ThemeAvailable != Themes.Standard);
		}
	}

//
//	public Texture themeThanksGivingOn;
//	public Texture themeThanksGivingOff;
//
//	public Texture themeChristmasOn;
//	public Texture themeChristmasOff;

	private Rect rect = new Rect();
	private Rect themeRect = new Rect();

	// Use this for initialization
	private bool DoButton (Texture Texture) {
		float OffsetUI = Mathf.Min (Screen.height, Screen.width) / 30;

		rect.x = OffsetUI;
		//rect.y = Screen.height - (OffsetUI + OffsetUI * 7 * 2 + OffsetUI);
		rect.y = OffsetUI;
		rect.width = OffsetUI * 7;
		rect.height = OffsetUI * 7;

		themeRect.x = rect.x + OffsetUI;
		themeRect.y = rect.y + OffsetUI;
		themeRect.width = OffsetUI * 5;
		themeRect.height = OffsetUI * 5;


		bool RetVal = Utility_OnGUI.Button (rect, Utility_CurrentTheme.ThemeEnabled ? this.buttonOn : this.buttonOff);
		GUI.DrawTexture (themeRect, Texture);
		return RetVal;
	}
	
	// Update is called once per frame
	void OnGUI () {
		switch (Utility_CurrentTheme.ThemeAvailable) {
		case Themes.Halloween:
			//Texture Texture = Utility_CurrentTheme.ThemeEnabled ? this.themeHalloweenOn : this.themeHalloweenOff;
			//Debug.Log ("rect: " + rect + " texture at: " + Texture );
			//if (DoButton(Utility_CurrentTheme.ThemeEnabled ? this.themeHalloweenOn : this.themeHalloweenOff)) {
			if (DoButton(this.themeHalloweenOn)) {
				Utility_CurrentTheme.Toggle();
				foreach (GameObject themeGameObject in themeGameObjects) {
					themeGameObject.SetActive(Utility_CurrentTheme.ThemeEnabled);
				}
			}
			break;
		}
	}
}

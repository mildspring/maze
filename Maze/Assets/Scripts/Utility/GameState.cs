﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameState {
	public static GameState current;

	public GameState() {
		structureVersion = 1;
	}
	public int structureVersion;
	public Position playerPosition;
	public int playerRotation;
	public List<Wall> wallsLeft;
	public List<DoorPositionSerializable> doorPositions;
	public Position finalPosition;
	public bool haveKey;
	public List<GameObjectPosition> pathArrows;
	public int timeElapsed;

	public override string ToString ()
	{
		return string.Format ("[GameState] " + structureVersion);
	}
}

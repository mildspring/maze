﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Utility_MusicQueue : MonoBehaviour {
	/// in seconds
	public int eachSoundSeconds = 0;

	List<AudioClip> sounds;
	int playIndex = 0;
	bool play = false;

	DateTime nextSound;

	void Start() {
		sounds = new List<AudioClip> ();
		nextSound = DateTime.Now;
	}

	public void Add(AudioClip AudioClip) {
		sounds.Add (AudioClip);
	}

	public void Play() {
		play = true;
	}

	void Update() {
		if (play) {
			if (Utility_Music.SoundOn) {
				if (playIndex < sounds.Count) {
					if (playIndex == 0 || !GetComponent<AudioSource>().isPlaying) {
						if (DateTime.Now >= nextSound) {
							GetComponent<AudioSource>().clip = sounds[playIndex];
							GetComponent<AudioSource>().Play ();
							nextSound = DateTime.Now.AddSeconds(this.eachSoundSeconds);
							++playIndex;
						}
						//else {
						//}
					}
				}
				else {
					play = false;
					playIndex = 0;
				}
			}
			else {
				if (play) {
					playIndex = 0;
				}
				play = false;
			}
		}
	}
}

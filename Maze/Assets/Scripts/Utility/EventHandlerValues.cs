﻿using UnityEngine;
using System.Collections;

public enum EventHandlerValues
{
	LEVEL_FINISHED_SUCCESSFULLY,
	LEVEL_FINISHED_UNSUCCESSFULLY,
	LEVEL_NEW_UNLOCKED,
	BUTTON_FACEBOOK,
	BUTTON_AWARDS,
	BUTTON_LEADERBOARD,
}

﻿using UnityEngine;
using System.Collections;

public delegate void MoveLerpObjectDone();

public class MoveLerpObject : MonoBehaviour {
	public MoveLerpObjectDone moveLerpObjectDone = null;
	public float speed = 0.3f;
	public Vector3 finalPosition;

	private float startTime;	
	private Vector3 originalPosition;
	
	private float journeyLength;

	// Use this for initialization
	void OnEnable() {
		startTime = Time.time;
		
		originalPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
		journeyLength = Vector3.Distance(originalPosition, finalPosition);
	}
	
	// Update is called once per frame
	void Update () {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		//Debug.Log ("Transform = " + originalPosition + " to: " + finalPosition);

//		if (float.IsNaN (originalPosition.x)) {
//			Debug.Log ("transform.x is Nan");
//		}
//		if (float.IsNaN (originalPosition.y)) {
//			Debug.Log ("transform.y is Nan");
//		}
//		if (float.IsNaN (originalPosition.z)) {
//			Debug.Log ("transform.z is Nan");
//		}
//		if (float.IsNaN (finalPosition.x)) {
//			Debug.Log ("final transform.x is Nan");
//		}
//		if (float.IsNaN (finalPosition.y)) {
//			Debug.Log ("final transform.y is Nan");
//		}
//		if (float.IsNaN (finalPosition.z)) {
//			Debug.Log ("final transform.z is Nan");
//		}
		if (float.IsNaN (fracJourney)) {
			Debug.Log ("fracJourney is Nan");
		}
		else {
			transform.position = Vector3.Lerp (originalPosition, finalPosition, fracJourney);


			if (Vector3.Equals(transform.position, finalPosition)) {
				this.enabled = false;
				if (moveLerpObjectDone != null) {
					moveLerpObjectDone();
				}
			}
		}
	}
}

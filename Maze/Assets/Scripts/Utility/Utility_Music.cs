﻿using UnityEngine;
using System.Collections;

public class Utility_Music : MonoBehaviour {
	public Texture musicOnTexture;
	public Texture soundOnTexture;
	public Texture soundOffTexture;

	private static bool musicOn;
	private static bool soundOn;
	Rect rect;
	Rect rectSound;


	// Use this for initialization
	void Start () {
		rect = new Rect ();
		rectSound = new Rect ();

		musicOn = VarEnums.GetInt (VarEnums.MusicOn, 1) == 1;
		soundOn = VarEnums.GetInt (VarEnums.SoundOn, 1) == 1;

		if (musicOn && !soundOn) {
			// fixing impossible setup - perhaps when I added sound after music
			ToggleSound();
		}

		if (musicOn) {
			GetComponent<AudioSource>().Play ();
		}
	}

	void OnGUI () {
		float OffsetUI = Mathf.Min (Screen.height, Screen.width) / 30;
		//rect.x = OffsetUI + OffsetUI * 7 + OffsetUI;
		rect.x = Screen.width - (OffsetUI + OffsetUI * 4);
		rect.y = OffsetUI;
		rect.width = OffsetUI * 4;
		rect.height = OffsetUI * 4;
		if (soundOn) {
			if (musicOn) {
				if(Utility_OnGUI.Button(rect, soundOnTexture)) {
					ToggleMusic();
				}
				rectSound.x = rect.x + OffsetUI * 1.5f;
				rectSound.y = rect.y + OffsetUI * 1.5f;
				rectSound.width = OffsetUI * 2.5f;
				rectSound.height = OffsetUI * 2.5f;
				GUI.DrawTexture(rectSound, musicOnTexture);
			}
			else {
				if(Utility_OnGUI.Button(rect, soundOnTexture)) {
					ToggleSound();
				}
			}
		}
		else {
			if(Utility_OnGUI.Button(rect, musicOn ? soundOnTexture : soundOffTexture)) {
				ToggleSound();
				ToggleMusic();
			}
		}
	}

	
	public void ToggleMusic() {
		musicOn = !musicOn;
		VarEnums.SetInt (VarEnums.MusicOn, musicOn ? 1 : 0);

		if (musicOn) {
			GetComponent<AudioSource>().Play ();
		}
		else {
			GetComponent<AudioSource>().Stop ();
		}
	}

	public static void ToggleSound() {
		soundOn = !soundOn;
		VarEnums.SetInt (VarEnums.SoundOn, soundOn ? 1 : 0);
	}

	//public static bool MusicOn { get { return musicOn; } }
	public static bool SoundOn { get { return soundOn; } }
}

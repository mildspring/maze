﻿using UnityEngine;
using System.Collections;

public class Utility_LevelButtonsPopup : MonoBehaviour
{
	public Texture okTexture;
	public Texture cancelTexture;
	public Texture starTexture;
	public Texture backgroundTexture;
	public Texture bigBackgroundTexture;
	private GUIStyle guiStyle;
	private GUIStyle guiLevelStyle;
	private Utility_LevelButtonsPopupHalloween utility_LevelButtonsPopupHalloween;

	// Use this for initialization
	void Start ()
	{
		guiStyle = new GUIStyle ();
		guiStyle.fontSize = Screen.height / 14;
		guiStyle.normal.textColor = Color.blue;

		guiLevelStyle = new GUIStyle ();
		guiLevelStyle.fontSize = Screen.height / 14 / 3 * 5;
		guiLevelStyle.normal.textColor = Color.blue;
		guiLevelStyle.alignment = TextAnchor.MiddleCenter;

		utility_LevelButtonsPopupHalloween = GetComponent<Utility_LevelButtonsPopupHalloween> ();
	}

	public static string GetStarTime (int StarsSeconds, bool LongForm = false)
	{
		string StartTime = "";
		if (StarsSeconds >= 60)
		{
			//StartTime += "" + (StarsSeconds / 60) + (LongForm ? " min " : "m ");
			StartTime += "" + (StarsSeconds / 60) + (LongForm ? " m " : "m ");
		}
		//StartTime += "" + (StarsSeconds % 60) + (LongForm ? " sec" : "s");
		StartTime += "" + (StarsSeconds % 60) + (LongForm ? " s" : "s");
		return StartTime;
	}
	
	public void Popup (int Level, int ThreeStarsSeconds, int TwoStarsSeconds, int OneStarSeconds, int BestTime, string ID, Utility_Menu_OKButton OKCallback, Utility_Menu_CancelButton CancelCallback)
	{
		utility_LevelButtonsPopupHalloween.Popup (Level, ThreeStarsSeconds, TwoStarsSeconds, OneStarSeconds, BestTime, ID, OKCallback, CancelCallback);
	}
}

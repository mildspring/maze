﻿using UnityEngine;
using System.Collections;

public class VarEnums {
	public const string LevelTimePrefix = "lvlTime";
	public const string PathOwned = "pathOwned";
	public const string NextHighestLevel = "nextHighestLevel";
	public const string Width = "width";
	public const string Height = "height";
	public const string Doors = "doors";
	public const string NextLevel = "nextLevel";
	public const string NextPage = "nextPage";
	public const string GameInProgress = "gameInProgress";
	public const string MusicOn = "musicOn";
	public const string SoundOn = "soundOn";
	public const string TimeElapsed = "timeElapsed";
	public const string ThemeEnabled = "themeEnabled";
	//public const string TotalStars = "totalStars";

	public static void SetInt(string Name, int Value) {
		PlayerPrefs.SetInt (Name, Value);
	}

	public static int GetInt(string Name) {
		return PlayerPrefs.GetInt (Name);
	}

	public static int GetInt(string Name, int DefaultValue) {
		return PlayerPrefs.GetInt (Name, DefaultValue);
	}
}

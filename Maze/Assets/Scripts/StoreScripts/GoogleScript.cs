﻿using UnityEngine;
using System.Collections;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GoogleScript : MonoBehaviour
{
	private static bool performedAuthenticate = false;

	//public Utility_Menu utility_Menu;
	public GameObject eventHandlerObject;
	private EventHandler eventHandler;

	void Awake ()
	{
		//if (StoreDetector.store != Stores.GOOGLE)
		{
			this.enabled = false;
			return;
		}

		/*if (!performedAuthenticate)
		{
			performedAuthenticate = true;

			// recommended for debugging:
			//PlayGamesPlatform.DebugLogEnabled = true;
		
			// Activate the Google Play Games platform
			//PlayGamesPlatform.Activate ();

			// authenticate user:
			Social.localUser.Authenticate ((bool success) => {
				// handle success or failure
				Debug.Log ("Authenticate returned: " + success);
				UpdateAchievements ();
			});
		}*/
	}

	private void UpdateAchievements ()
	{
		{
			int TotalStars = Utility_LevelButtons.GetTotalStars (); // VarEnums.GetInt(VarEnums.TotalStars, 0);
			int NextHighestLevel = VarEnums.GetInt (VarEnums.NextHighestLevel);

			// post score 12345 to leaderboard ID "Cfji293fjsie_QA")
			if (NextHighestLevel > 1)
			{
				Social.ReportScore (NextHighestLevel - 1, GoogleID.LEADERBOARD_ID, (bool success) => {
					// handle success or failure
				});
			}

			if (NextHighestLevel > 20)
			{
				// unlock achievement (achievement ID "Cfjewijawiu_QA")
				Social.ReportProgress (GoogleID.ACHIEVEMENT_LEVEL20, 100.0f, (bool success) => {
					// handle success or failure
				});
			}
			if (NextHighestLevel > 40)
			{
				// unlock achievement (achievement ID "Cfjewijawiu_QA")
				Social.ReportProgress (GoogleID.ACHIEVEMENT_LEVEL40, 100.0f, (bool success) => {
					// handle success or failure
				});
			}
			if (NextHighestLevel > 60)
			{
				// unlock achievement (achievement ID "Cfjewijawiu_QA")
				Social.ReportProgress (GoogleID.ACHIEVEMENT_LEVEL60, 100.0f, (bool success) => {
					// handle success or failure
				});
			}
			Debug.Log ("Google, reporting TotalStars=" + TotalStars);
			// not sure how this works, i believe the parameter should be 100.0f, but it seems to work in google
			Social.ReportProgress (GoogleID.ACHIEVEMENT_STARS10, TotalStars / 10, (bool success) => {
				// handle success or failure
			});
			Social.ReportProgress (GoogleID.ACHIEVEMENT_STARS60, TotalStars / 60, (bool success) => {
				// handle success or failure
			});
			Social.ReportProgress (GoogleID.ACHIEVEMENT_STARS120, TotalStars / 120, (bool success) => {
				// handle success or failure
			});
			Social.ReportProgress (GoogleID.ACHIEVEMENT_STARS180, TotalStars / 180, (bool success) => {
				// handle success or failure
			});
		}
	}

	private void OnEnable ()
	{
		Debug.Log ("GoogleScript OnEnable");
		eventHandler = eventHandlerObject.GetComponent<EventHandler> ();
		eventHandler.AddEvent (EventHandlerValues.BUTTON_LEADERBOARD, ShowLeaderboardsOverlay);
		eventHandler.AddEvent (EventHandlerValues.BUTTON_AWARDS, ShowAchievementsOverlay);

		//utility_Menu = eventHandler.GetComponent<Utility_Menu> ();

		if (performedAuthenticate)
		{
			UpdateAchievements ();
		}
	}

	private void ShowLeaderboardsOverlay (EventHandlerValues eventHandlerValue, int Fake)
	{
		Debug.Log ("Got into Google's ShowLeaderboardsOverlay");
		//((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (GoogleID.LEADERBOARD_ID);
	}
	
	private void ShowAchievementsOverlay (EventHandlerValues eventHandlerValue, int Fake)
	{
		Debug.Log ("Got into Google's ShowAchievementsOverlay");
		// show achievements UI
		//((PlayGamesPlatform)Social.Active).ShowAchievementsUI ();
		//Social.ShowAchievementsUI ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class GoogleID {
	public static string LEADERBOARD_ID = "CgkIxaTb464ZEAIQBA";
	public static string ACHIEVEMENT_LEVEL20 = "CgkIxaTb464ZEAIQAQ";
	public static string ACHIEVEMENT_LEVEL40 = "CgkIxaTb464ZEAIQAg";
	public static string ACHIEVEMENT_LEVEL60 = "CgkIxaTb464ZEAIQAw";
	public static string ACHIEVEMENT_STARS10 = "CgkIxaTb464ZEAIQBQ";
	public static string ACHIEVEMENT_STARS60 = "CgkIxaTb464ZEAIQBg";
	public static string ACHIEVEMENT_STARS120 = "CgkIxaTb464ZEAIQBw";
	public static string ACHIEVEMENT_STARS180 = "CgkIxaTb464ZEAIQCA";
}

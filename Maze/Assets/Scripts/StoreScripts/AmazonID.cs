﻿using UnityEngine;
using System.Collections;

public class AmazonID
{
	public static string LEADERBOARD_ID = "maze_unlimited_ladderboard";
	public static string ACHIEVEMENT_LEVEL20 = "level20";
	public static string ACHIEVEMENT_LEVEL40 = "level40";
	public static string ACHIEVEMENT_LEVEL60 = "level60";
	public static string ACHIEVEMENT_STARS10 = "stars10";
	public static string ACHIEVEMENT_STARS60 = "stars60";
	public static string ACHIEVEMENT_STARS120 = "stars120";
	public static string ACHIEVEMENT_STARS180 = "stars180";
}

﻿using UnityEngine;
using System.Collections;

public class LevelProperties {
	private int number;
	private int doors;
	private int x;
	private int y;
	private int secondsThreeStars;
	private int secondsTwoStars;
	private int secondsOneStar;

	public LevelProperties(int Number, int Doors, int X, int Y, int SecondsThreeStars, int SecondsTwoStars, int SecondsOneStar) {
		number = Number;
		doors = Doors;
		x = X;
		y = Y;

		secondsThreeStars = SecondsThreeStars;
		secondsTwoStars = SecondsTwoStars;
		secondsOneStar = SecondsOneStar;
	}

	public LevelProperties(int Number, int Doors, int X, int Y) : this(Number, Doors, X, Y, (int) (Mathf.Ceil(X*Y / 5) * 5), (int) (Mathf.Ceil((float) (X*Y * 1.5 / 5)) * 5),(int) (Mathf.Ceil((float) (X*Y * 2 / 5)) * 5)) {
	}

	public int Number { get { return number; } }
	public int Doors { get { return doors; } }
	public int X { get { return x; } }
	public int Y { get { return y; } }
	public int SecondsThreeStars { get { return secondsThreeStars; } }
	public int SecondsTwoStars { get { return secondsTwoStars; } }
	public int SecondsOneStar { get { return secondsOneStar; } }

	public override string ToString () {
		return string.Format ("[LevelProperties: Number={0}, Doors={1}, X={2}, Y={3}, SecondsThreeStars={4}, SecondsTwoStars={5}, SecondsOneStar={6}]", Number, Doors, X, Y, SecondsThreeStars, SecondsTwoStars, SecondsOneStar);
	}

}

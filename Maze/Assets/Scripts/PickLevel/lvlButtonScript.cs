﻿using UnityEngine;

public class lvlButtonScript : MonoBehaviour {
	public UnityEngine.UI.Image levelLock;
	public UnityEngine.UI.Text levelText;
	bool colorBlue = true;

	public void OnMouseDown() {
		Debug.Log ("lvlButtonScript: OnMouseDown");
		levelText.text = (int.Parse(levelText.text) - 1).ToString();
		colorBlue = !colorBlue;
		((UnityEngine.UI.Text)GetComponent (typeof(UnityEngine.UI.Text))).color = colorBlue ? Color.blue : Color.red;
	}
}

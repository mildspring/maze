﻿using UnityEngine;

public class LevelFacebook : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		EventHandler EventHandler = GetComponent<EventHandler> ();
		EventHandler.AddEvent (EventHandlerValues.BUTTON_FACEBOOK, FacebookHandler);
	}
	
	private void FacebookHandler (EventHandlerValues EventHandlerValue, int Fake)
	{
		Debug.Log ("Got into FacebookHandler");
		//Application.OpenURL("https://www.facebook.com/MazeUnlimited");

		if (checkPackageAppIsPresent ("com.facebook.katana"))
		{
			Application.OpenURL ("fb://page/554145504713328"); //there is Facebook app installed so let's use it
		}
		else
		{
			Application.OpenURL ("https://www.facebook.com/MazeUnlimited"); // no Facebook app - use built-in web browser
		}
	}


	// got the method from http://answers.unity3d.com/questions/777838/unity-like-facebook-page-through-app.html
	private bool checkPackageAppIsPresent (string package)
	{
        try
        {
			AndroidJavaClass up = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
			AndroidJavaObject ca = up.GetStatic<AndroidJavaObject> ("currentActivity");
			AndroidJavaObject packageManager = ca.Call<AndroidJavaObject> ("getPackageManager");
		
			//take the list of all packages on the device
			AndroidJavaObject appList = packageManager.Call<AndroidJavaObject> ("getInstalledPackages", 0);
			int num = appList.Call<int> ("size");
			for (int i = 0; i < num; i++)
			{
				AndroidJavaObject appInfo = appList.Call<AndroidJavaObject> ("get", i);
				string packageNew = appInfo.Get<string> ("packageName");
				if (packageNew.CompareTo (package) == 0)
				{
					return true;
				}
			}
			return false;
		}
		catch (System.Exception)
        {
			return false;
		}
	}
}

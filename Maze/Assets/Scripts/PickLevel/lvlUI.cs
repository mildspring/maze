using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class lvlUI : MonoBehaviour
{
	//public GUIText guiText;
	public Texture onGuiConfig;
	public Texture creditsTexture;
	public Texture leftArrow;
	public Texture rightArrow;
	public Texture lockTexture;
	public Texture textureFacebook;
	public Texture textureLeaderboard;
	public Texture textureAwards;
	public int showRateUsAtLevel = 6;
	private int maxLevel = 1;
	public int maxPage = 4;
	private int nextLevel = 0;
	private int nextLevelBestTime = 999;
	private int nextPage = 0;
	private Rect rect;
	private bool displayLevelPopup = false;
	private Utility_LevelButtonsPopup utility_LevelButtonsPopup;
	private LevelSetup levelSetup;
	private bool possibleExit = false;
	private Utility_Menu Utility_Menu;
	private ButtonBlock buttonBlock;
	private Utility_LevelButtons utility_LevelButtons;

	void Start ()
	{
		Utility_Menu = GetComponent<Utility_Menu> ();
		utility_LevelButtons = GetComponent<Utility_LevelButtons> ();
		nextLevel = VarEnums.GetInt (VarEnums.NextLevel, 0);
		nextPage = VarEnums.GetInt (VarEnums.NextPage, 1);
		rect = new Rect ();

		utility_LevelButtonsPopup = GetComponent<Utility_LevelButtonsPopup> ();
		levelSetup = GetComponent<LevelSetup> ();

		maxLevel = Mathf.Min (VarEnums.GetInt (VarEnums.NextHighestLevel, 1), GetComponent<LevelSetup> ().TotalLevels);

		if (showRateUsAtLevel <= maxLevel)
		{
			//Debug.Log ("showRateUsAtLevel is " + showRateUsAtLevel + " maxLevel is " + maxLevel);
			GetComponent<RateUsButton> ().enabled = true;
		}

		buttonBlock = new ButtonBlock (GetComponent<EventHandler> ());
		//buttonBlock.AddButton (textureAwards, EventHandlerValues.BUTTON_AWARDS);
		//buttonBlock.AddButton (textureLeaderboard, EventHandlerValues.BUTTON_LEADERBOARD);
		buttonBlock.AddButton (textureFacebook, EventHandlerValues.BUTTON_FACEBOOK);

		if (VarEnums.GetInt (VarEnums.GameInProgress, 0) == 1)
		{
			Utility_Menu.MessageOnScreen = "Resuming Game\n...";

            StartCoroutine(LoadSceneAsync());
		}

		//maxLevel = 90; //for testing
	}

    private IEnumerator LoadSceneAsync()
    {
        var waitToLoad = SceneManager.LoadSceneAsync("Level");
        while (!waitToLoad.isDone)
        {
            yield return null;
        }
    }


    private void OKCallback (string ID)
	{
		displayLevelPopup = false;
		int NextLevel = VarEnums.GetInt (VarEnums.NextLevel, 1);
		GetComponent<LevelSetup> ().PropertiesToPrefs (NextLevel);
		Utility_Menu.MessageOnScreen = "Loading Level\n...";
        StartCoroutine(LoadSceneAsync());
	}

	private void CancelCallback (string ID)
	{	
		displayLevelPopup = false;
	}

	void Update ()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			if (Input.GetKey (KeyCode.Escape))
			{
				possibleExit = true;
			}
		}
	}
	
	void OnGUI ()
	{
		if (displayLevelPopup)
		{
			utility_LevelButtonsPopup.Popup (nextLevel, levelSetup.Level (nextLevel).SecondsThreeStars, levelSetup.Level (nextLevel).SecondsTwoStars, levelSetup.Level (nextLevel).SecondsOneStar, nextLevelBestTime, "level", OKCallback, CancelCallback);
			return;
		}
		if (possibleExit)
		{
			Utility_Menu.Menu ("exitPrompt", "Do you want to exit?", this.MenuOK, this.MenuCancel);
			return;
		}

		float OffsetUI = Mathf.Min (Screen.height, Screen.width) / 30;
		rect.x = OffsetUI;
		rect.y = Screen.height - OffsetUI * 7;
		rect.width = OffsetUI * 6;
		rect.height = OffsetUI * 6;
		if (Utility_OnGUI.Button (rect, onGuiConfig))
		{
			possibleExit = true;
		}

		rect.x += OffsetUI * 7;
//		if (Utility_OnGUI.Button (rect, this.creditsTexture))
//		{
//			Application.LoadLevel ("Credits");
//		}

		int ButtonSize = Screen.width / 10;
		int MaxLevel = maxLevel;
		int NewButtonSelected;

		//NewButtonSelected = utility_LevelButtons.Buttons (5, 4, 20 * (nextPage - 1) + 1, MaxLevel, nextLevel, ButtonSize / 10 + ButtonSize, ButtonSize / 10 + ButtonSize);
		NewButtonSelected = utility_LevelButtons.Buttons (5, 3, 15 * (nextPage - 1) + 1, MaxLevel, nextLevel, ButtonSize / 10 + ButtonSize, ButtonSize / 10 + ButtonSize, 50);

		// draw a button to the left if we're on page 2 or higher
		if (nextPage > 1)
		{
			rect.x = ButtonSize / 10;
			rect.y = Screen.height / 2 - ButtonSize / 2;
			rect.width = ButtonSize;
			rect.height = ButtonSize;
			if (Utility_OnGUI.Button (rect, this.leftArrow))
			{
				--nextPage;
			}
		}

		rect.x = Screen.width - (ButtonSize / 10 + ButtonSize);
		rect.y = Screen.height / 2 - ButtonSize / 2;
		rect.width = ButtonSize;
		rect.height = ButtonSize;

		bool NextIsLocked = (nextPage == this.maxPage);
		if (Utility_OnGUI.Button (rect, this.rightArrow) && !NextIsLocked)
		{
			++nextPage;
		}

		if (NextIsLocked)
		{
			int LockSize = ButtonSize / 3;
			rect.x += LockSize;
			rect.y += LockSize;
			rect.width = LockSize;
			rect.height = LockSize;
			GUI.DrawTexture (rect, lockTexture);
		}

		if (NewButtonSelected > 0)
		{
			nextLevel = NewButtonSelected;
			VarEnums.SetInt (VarEnums.NextLevel, nextLevel);
			VarEnums.SetInt (VarEnums.NextPage, nextPage);
			Debug.Log ("Starting level " + (nextLevel));

			nextLevelBestTime = VarEnums.GetInt (VarEnums.LevelTimePrefix + nextLevel, -1);
			displayLevelPopup = true;
		}

		buttonBlock.CallOnGUI ();
	}

	private void MenuOK (string ID)
	{
		Debug.Log ("MenuOK: " + ID);
		if (ID == "exitPrompt")
		{
			VarEnums.SetInt (VarEnums.GameInProgress, 0);
			Application.Quit ();
		}
		DisableMenu ();
	}

	private void MenuCancel (string ID)
	{
		Debug.Log ("MenuCancel: " + ID);
		DisableMenu ();
	}
	
	private void DisableMenu ()
	{
		possibleExit = false;
	}
}

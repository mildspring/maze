﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSetup : MonoBehaviour {
	//public int totalLevels;

	private List<LevelProperties> levelProperties;

	// Use this for initialization
	void Awake () {
		levelProperties = new List<LevelProperties>();
		levelProperties.Add (new LevelProperties (1, 0, 5, 5));
		levelProperties.Add (new LevelProperties (2, 1, 6, 6));
		levelProperties.Add (new LevelProperties (3, 1, 6, 6));
		levelProperties.Add (new LevelProperties (4, 1, 7, 6));
		levelProperties.Add (new LevelProperties (5, 1, 7, 6));
		levelProperties.Add (new LevelProperties (6, 1, 7, 7));
		levelProperties.Add (new LevelProperties (7, 2, 7, 7));
		levelProperties.Add (new LevelProperties (8, 2, 8, 7));
		levelProperties.Add (new LevelProperties (9, 2, 8, 7));
		levelProperties.Add (new LevelProperties (10, 2, 8, 8));
		levelProperties.Add (new LevelProperties (11, 2, 8, 8));
		levelProperties.Add (new LevelProperties (12, 3, 9, 8));
		levelProperties.Add (new LevelProperties (13, 3, 9, 8));
		levelProperties.Add (new LevelProperties (14, 3, 9, 9));
		levelProperties.Add (new LevelProperties (15, 3, 9, 9));
		levelProperties.Add (new LevelProperties (16, 3, 10, 9));
		levelProperties.Add (new LevelProperties (17, 3, 10, 9));
		levelProperties.Add (new LevelProperties (18, 3, 10, 10));
		levelProperties.Add (new LevelProperties (19, 3, 10, 10));
		levelProperties.Add (new LevelProperties (20, 3, 11, 10));
		levelProperties.Add (new LevelProperties (21, 3, 11, 10));
		levelProperties.Add (new LevelProperties (22, 3, 11, 11));
		levelProperties.Add (new LevelProperties (23, 3, 11, 11));
		levelProperties.Add (new LevelProperties (24, 3, 12, 11));
		levelProperties.Add (new LevelProperties (25, 3, 12, 11));
		levelProperties.Add (new LevelProperties (26, 4, 12, 12));
		levelProperties.Add (new LevelProperties (27, 4, 12, 12));
		levelProperties.Add (new LevelProperties (28, 4, 13, 12));
		levelProperties.Add (new LevelProperties (29, 4, 13, 12));
		levelProperties.Add (new LevelProperties (30, 4, 13, 13));
		levelProperties.Add (new LevelProperties (31, 4, 13, 13));
		levelProperties.Add (new LevelProperties (32, 4, 14, 13));
		levelProperties.Add (new LevelProperties (33, 4, 14, 13));
		levelProperties.Add (new LevelProperties (34, 4, 14, 14));
		levelProperties.Add (new LevelProperties (35, 4, 14, 14));
		levelProperties.Add (new LevelProperties (36, 4, 15, 14));
		levelProperties.Add (new LevelProperties (37, 4, 15, 14));
		levelProperties.Add (new LevelProperties (38, 4, 15, 15));
		levelProperties.Add (new LevelProperties (39, 4, 15, 15));
		levelProperties.Add (new LevelProperties (40, 5, 16, 15));
		levelProperties.Add (new LevelProperties (41, 5, 16, 15));
		levelProperties.Add (new LevelProperties (42, 5, 16, 16));
		levelProperties.Add (new LevelProperties (43, 5, 16, 16));
		levelProperties.Add (new LevelProperties (44, 5, 17, 16));
		levelProperties.Add (new LevelProperties (45, 5, 17, 16));
		levelProperties.Add (new LevelProperties (46, 5, 17, 17));
		levelProperties.Add (new LevelProperties (47, 5, 17, 17));
		levelProperties.Add (new LevelProperties (48, 5, 18, 17));
		levelProperties.Add (new LevelProperties (49, 5, 18, 17));
		levelProperties.Add (new LevelProperties (50, 6, 18, 18));
		levelProperties.Add (new LevelProperties (51, 6, 18, 18));
		levelProperties.Add (new LevelProperties (52, 6, 19, 18));
		levelProperties.Add (new LevelProperties (53, 6, 19, 18));
		levelProperties.Add (new LevelProperties (54, 5, 19, 19));
		levelProperties.Add (new LevelProperties (55, 5, 19, 19));
		levelProperties.Add (new LevelProperties (56, 5, 20, 19));
		levelProperties.Add (new LevelProperties (57, 5, 20, 19));
		levelProperties.Add (new LevelProperties (58, 4, 20, 20));
		levelProperties.Add (new LevelProperties (59, 4, 20, 20));
		levelProperties.Add (new LevelProperties (60, 2, 20, 20));

		AddLevelProperties (61, 90, 7, 20, 20, 4, 10);
			
		// debug
//		foreach (LevelProperties LevelProperties in levelProperties) {
//			Debug.Log (LevelProperties);
//		}
	}

	private void AddLevelProperties(int StartingLevel, int FinishLevel, int StartingDoors, int StartingX, int StartingY, int IncreaseEveryN, int IcreaseDoorsEveryN) {
//		int NextX = StartingLevel + IncreaseEveryN / 2;
//		int NextY = StartingLevel + IncreaseEveryN;
//		int NextDoor = StartingLevel + IcreaseDoorsEveryN;
		for (int CurrentLevel = StartingLevel; CurrentLevel <= FinishLevel; ++CurrentLevel) {
			int CurrentDoors = StartingDoors + (CurrentLevel - StartingLevel) / IcreaseDoorsEveryN;
			int CurrentX = StartingX + Mathf.CeilToInt((CurrentLevel - StartingLevel + IncreaseEveryN/2) / IncreaseEveryN);
			int CurrentY = StartingY + Mathf.CeilToInt((CurrentLevel - StartingLevel) / IncreaseEveryN);
			levelProperties.Add (new LevelProperties (CurrentLevel, CurrentDoors, CurrentX, CurrentY));
		}
	}
	
	public LevelProperties Level(int Level) {
		if (Level - 1 >= levelProperties.Count) {
			Debug.Log ("Internal Error ... shouldn't be here, level is " + Level + " but the top level is " + levelProperties.Count);
			return levelProperties[levelProperties.Count - 1];
		}
		else if (Level <= 0) {
			Debug.Log ("Internal Error ... shouldn't be here, level is " + Level + " but has to be above 0");
			return levelProperties[0];
		}
		return levelProperties [Level-1];
	}
	public int TotalLevels { get { return levelProperties.Count; } }

	public void PropertiesToPrefs(int Level) {
		VarEnums.SetInt (VarEnums.NextLevel, Level);
		VarEnums.SetInt (VarEnums.Doors, levelProperties [Level - 1].Doors);
		VarEnums.SetInt (VarEnums.Width, levelProperties [Level - 1].X);
		VarEnums.SetInt (VarEnums.Height, levelProperties [Level - 1].Y);
	}
}

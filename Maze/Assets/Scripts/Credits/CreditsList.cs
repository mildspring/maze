﻿using UnityEngine;
using System.Collections.Generic;

public class CreditsList : MonoBehaviour {
	List<CreditsItem> CreditsItems;

	void Awake () {
		CreditsItems = new List<CreditsItem> ();
		// assetstore
		CreditsItems.Add (new CreditsItem ("Earthborn Troll", "Earthborn Troll", "https://www.assetstore.unity3d.com/en/#!/content/13541"));
		CreditsItems.Add (new CreditsItem ("fantasy-colorable", "Colorable Fantasy UI", "https://www.assetstore.unity3d.com/en/#!/content/7563"));
		CreditsItems.Add (new CreditsItem ("Ground textures pack", "Ground textures pack", "https://www.assetstore.unity3d.com/en/#!/content/13001"));
		CreditsItems.Add (new CreditsItem ("Metal textures pack", "Metal textures pack", "https://www.assetstore.unity3d.com/en/#!/content/12949"));

		// iconarchive
		CreditsItems.Add (new CreditsItem ("icons/iconarchive1", "Green Button", "http://www.iconarchive.com/show/I-like-buttons-3a-icons-by-mazenl77/Perspective-Button-Go-icon.html"));
		CreditsItems.Add (new CreditsItem ("icons/iconarchive2", "Red Button", "http://www.iconarchive.com/show/I-like-buttons-3a-icons-by-mazenl77/Perspective-Button-Stop-icon.html"));

		// clipart best
		CreditsItems.Add (new CreditsItem ("dT6ee788c", "Red Arrow", "http://www.clipartbest.com/clipart-dT6ee788c"));
		CreditsItems.Add (new CreditsItem ("left_arrow", "Left Arrow", "http://www.clipartbest.com/clipart-4nTExzziA"));
		CreditsItems.Add (new CreditsItem ("ncBgaKAcA", "Gear", "http://www.clipartbest.com/clipart-ncBgaKAcA"));
		CreditsItems.Add (new CreditsItem ("right_arrow", "Right Arrow", "http://www.clipartbest.com/clipart-RTG8GGkTL"));
		CreditsItems.Add (new CreditsItem ("9T446rjTE", "Check", "http://www.clipartbest.com/clipart-9T446rjTE"));
		CreditsItems.Add (new CreditsItem ("nTBGRzArc", "Cancel", "http://www.clipartbest.com/clipart-nTBGRzArc"));

		CreditsItems.Add (new CreditsItem ("y9TzAMjiE", "Parchment", "http://www.clipartbest.com/clipart-y9TzAMjiE"));

		// end of 7 by 2

		// findicons
		CreditsItems.Add (new CreditsItem ("arrow_right", "Right Green Arrow", "http://findicons.com/icon/69574/arrow_right?id=69574"));
		CreditsItems.Add (new CreditsItem ("lock_closed", "Lock", "http://findicons.com/icon/251605/lock_closed?id=252526"));
		CreditsItems.Add (new CreditsItem ("star", "Star", "http://findicons.com/icon/79729/star?id=80067"));
		CreditsItems.Add (new CreditsItem ("search", "Search", "http://findicons.com/icon/2985/search?id=389986"));

		CreditsItems.Add (new CreditsItem ("soundon", "Sound On", "http://findicons.com/icon/210361/sound"));
		CreditsItems.Add (new CreditsItem ("soundoff", "Sound Off", "http://findicons.com/icon/210441/no_sound"));
		CreditsItems.Add (new CreditsItem ("music_note_orange", "Music Note", "http://findicons.com/icon/547838/music_note_orange?id=547993"));
		CreditsItems.Add (new CreditsItem ("google_play", "Google Play", "http://findicons.com/icon/585088/google_play?id=585202"));

		// clker
		CreditsItems.Add (new CreditsItem ("run-black-man-md", "Running Man", "http://www.clker.com/clipart-running-man-black.html"));
		CreditsItems.Add (new CreditsItem ("stickman-md", "Standing Man", "http://www.clker.com/clipart-stickman.html"));

		CreditsItems.Add (new CreditsItem ("assets", "Sounds", "http://bleepblopaudio.com/"));

		CreditsItems.Add (new CreditsItem ("facebook_icons", "Facebook Icon", "https://www.facebookbrand.com/"));

		// end of 7

		CreditsItems.Add (new CreditsItem ("sk2", "Key", "http://www.turbosquid.com/FullPreview/Index.cfm/ID/484900"));

		// freesound
		CreditsItems.Add (new CreditsItem ("167877gmarchisiokey-drop", "Sound", "http://freesound.org/people/gmarchisio/sounds/167877/"));
		CreditsItems.Add (new CreditsItem ("Down (Dark Metal / Electronic Loop)", "Track Name: \" Down (Dark Metal / Electronic Loop) \"\n" +
											"Composer: Marcus Dellicompagni; Website: www.RedAudioUK.com",
											"http://freesound.org/people/dingo1/sounds/174589/"));

		//CreditsItems.Add (new CreditsItem ("", ""));

		// Halloween
		CreditsItems.Add (new CreditsItem ("Ghost", "Ghost - Maricris Villareal", ""));
		CreditsItems.Add (new CreditsItem ("halloween/Spider Green", "Green Spider", "https://www.assetstore.unity3d.com/en/#!/content/11869"));
		CreditsItems.Add (new CreditsItem ("Necromancer GUI", "Necromancer GUI", "https://www.assetstore.unity3d.com/en/#!/content/366"));
		CreditsItems.Add (new CreditsItem ("bat-icon", "Bat Icon", "http://www.iconarchive.com/show/halloween-icons-by-gcds/bat-icon.html"));
		CreditsItems.Add (new CreditsItem ("pumpkin", "Pumpkin", "https://www.assetstore.unity3d.com/en/#!/content/12185"));
	}

	public List<CreditsItem> GetCreditsItems() {
		return CreditsItems;
	}

}

public class CreditsItem {
	public string id;
	public string description;
	public string link;

	public CreditsItem (string ID, string Description, string Link)  {
		id = ID;
		description = Description;
		link = Link;
	}
}

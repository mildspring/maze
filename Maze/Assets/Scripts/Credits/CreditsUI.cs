﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CreditsUI : MonoBehaviour {
	public Texture rightArrow;
	public int linksPerPage;

	private List <CreditsItem> creditsItems;
	private Rect rect;
	private int startsAt;

	// Use this for initialization
	void Start () {
		creditsItems = GetComponent<CreditsList> ().GetCreditsItems ();
		rect = new Rect ();
		startsAt = 0;
	}

	void OnGUI() {
		int ButtonSize = Screen.height / (linksPerPage + 2 );

		rect.x = Screen.width / 2;
		rect.y = Screen.height - ButtonSize - ButtonSize / (linksPerPage + 1);
		rect.width = ButtonSize;
		rect.height = ButtonSize;
		if (Utility_OnGUI.Button(rect, this.rightArrow)) {
			startsAt += linksPerPage;
			if (startsAt >= creditsItems.Count) {
				//Application.LoadLevel("PickLevel");
				SceneManager.LoadScene("PickLevel");
			}
		}
		if (startsAt < creditsItems.Count) {
			rect.x = 0;
			rect.y = ButtonSize / (linksPerPage + 1);
			rect.width = Screen.width;
			rect.height = ButtonSize;

			int Max = Mathf.Min (creditsItems.Count - startsAt, linksPerPage);
			for (int i = 0; i < Max; ++i) {
				if (Utility_OnGUI.Button(rect, creditsItems[i + startsAt].description)) {
					if (!string.IsNullOrEmpty(creditsItems[i + startsAt].link)) {
						Application.OpenURL(creditsItems[i + startsAt].link);
					}
				}
				rect.y += ButtonSize + ButtonSize / (linksPerPage + 1);
			}
		}

		rect.x = 0;
		rect.y = Screen.height - ButtonSize;
		rect.width = ButtonSize;
		rect.height = ButtonSize;
		Utility_OnGUI.Button (rect, "1.13");

	}
}

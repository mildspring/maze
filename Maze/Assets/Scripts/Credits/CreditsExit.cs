﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsExit : MonoBehaviour {

	void Update () {
		if (Application.platform == RuntimePlatform.Android) {
			if (Input.GetKey (KeyCode.Escape)) {
				//Application.LoadLevel("PickLevel");
				SceneManager.LoadScene("PickLevel");
				
				return;
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains images based on two websites:
/// Freeware:
/// http://www.iconarchive.com/show/I-like-buttons-3a-icons-by-mazenl77/Perspective-Button-Go-icon.html
/// http://www.iconarchive.com/show/I-like-buttons-3a-icons-by-mazenl77/Perspective-Button-Stop-icon.html
/// and
/// Creative Commons Attribution No Derivatives (by-nd)
/// http://findicons.com/icon/177267/lock_yellow?id=177516
/// 
/// font used: Georgia Bold Italic with 300 pixels
/// </summary>


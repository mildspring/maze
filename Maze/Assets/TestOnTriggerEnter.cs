using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestOnTriggerEnter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Current Time Scale: " + Time.timeScale);
    }

    // Update is called once per frame
    void Update()
    {
        print($"trigger is active {gameObject.name}");
    }

	void OnTriggerEnter(Collider other)
	{
		print($"collided1 with {other.name}");
	}

    void OnCollisionEnter(Collision other)
    {
        print($"collided2 with {other.gameObject.name}");
    }
}
